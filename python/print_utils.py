import json
import os
import datetime
import win32con
import win32print
import win32ui
from PIL import Image, ImageWin
from PIL import Image as PILImage
import qrcode

QR_URL = "https://forms.gle/1JzHjA4D7zkccnHJ9"
store_address = "1223 Surf Ave, Brooklyn, NY, 11224"
store_phone = ""

# import keys.json for modifiers
f = open("keys.json")
mod_keys = json.load(f)
f.close()


def format_json(order):
    '''
    Reformat the json data into a dictionary
        order: string in json format
    '''
    # format as json
    with open('print_order.json', 'w') as f:
        f.write(json.dumps(order, indent=2, ensure_ascii=False))
        f.close()
    
    # open and read json file
    f = open('print_order.json')
    data_dict = json.load(f)
    f.close()

    return data_dict


def print_receipt(WIDTH, data, printer_name, qr_url = QR_URL):
    '''
    Print the receipt using data in a dictionary
    Args:
        WIDTH: int, width of the receipt
        data: data: {summary: {"Item1": { 
                                    "ID": str, 
                                    "category": str,
                                    "quantity": int,
                                    "item": str,
                                    "size": str,
                                    "ice": str,
                                    "sugar": str,
                                    "toppings": [],
                                    "toppings_required": [],
                                    "toppings_additional": [],
                                    "milk": str,
                                    "price": float
                                    },
                        "Item2": {...}},
                payment: {  "subtotal": float,
                            "tax": float,
                            "total": float
                            },
                description: {  "order_id": str,
                                "order_type": str,
                                "order_source": str,
                                "cash": bool
                            }
                }
        qr_url: string, url for qr code
    '''
    top_margin = 10
    tab = 10
    bit = 2
    linespace = 0

    def align_center(text, font):
        nonlocal linespace
        dc.SelectObject(font)
        linespace+=dc.GetTextExtent(text)[1]
        text_position = ((WIDTH-dc.GetTextExtent(text)[0])//2, linespace)
        dc.TextOut(*text_position, text)
        # linespace+=dc.GetTextExtent(text)[1]

    def align_left(text, font):
        nonlocal linespace
        dc.SelectObject(font)
        linespace += dc.GetTextExtent(text)[1]
        header_position = (2*tab, linespace)
        dc.TextOut(*header_position, text)

    def tab_align_left(text, font):
        nonlocal linespace
        dc.SelectObject(font)
        linespace += dc.GetTextExtent(text)[1]
        header_position = (4*tab, linespace)
        dc.TextOut(*header_position, text)

    def align_right(text, font):
        nonlocal linespace
        dc.SelectObject(font)
        header_position = (WIDTH-(2*tab+dc.GetTextExtent(text)[0]), linespace)
        dc.TextOut(*header_position, text)

    def sec_align_right(text, font):
        nonlocal linespace
        dc.SelectObject(font)
        header_position = (WIDTH-(8*tab+dc.GetTextExtent(text)[0]), linespace)
        dc.TextOut(*header_position, text)

    def add_line():
        nonlocal linespace
        linespace+=lf
        line_position = (tab, linespace)  # Adjust as needed
        line_end_position = (WIDTH-tab, linespace)
        dc.MoveTo(*line_position)
        dc.LineTo(*line_end_position)

    def add_line2(font):
        nonlocal linespace
        linespace += lf
        dc.SelectObject(font)
        line_style = "_ "
        num_dots = (WIDTH-tab) // (dc.GetTextExtent(line_style)[0])
        line_text = (line_style * int(num_dots))
        line_start_x = tab
        dc.TextOut(line_start_x, linespace, line_text)

    def add_qrcode(url):
        nonlocal linespace
        linespace += lf//2
        qr = qrcode.QRCode(
            version=1, #control qrcode size(1-40)
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=4,
            border=4,
        )
        qr.add_data(url)
        qr.make(fit=True)
        qr_code = qr.make_image(fill_color="black", back_color="white")

        qr_code_width, qr_code_height = qr_code.size
        qr_code_position = ((WIDTH - qr_code_width) // 2, linespace)

        dib = ImageWin.Dib(qr_code)
        dib.draw(dc.GetHandleOutput(), (qr_code_position[0], qr_code_position[1], 
                                         qr_code_position[0] + qr_code_width, 
                                         qr_code_position[1] + qr_code_height))

        linespace += (qr_code_height//2)+lf
        # linespace+=lf
        
    # define font
    font_info = {
        "name": "Consolas",
        "height": 28,
    }
    order_num_font_info= {
        "name": "Consolas",
        "height": 50,
        "weight": 700
    }
    dining_font_info= {
        "name": "Consolas",
        "height": 35,
        "weight": 400
    }
    dotted_line_info= {
        "name": "Consolas",
        "height":15
    }
    font_info_i1 = {
        "name": "Consolas",
        "height": 21,
        'italic':1
    }

    font = win32ui.CreateFont(font_info)
    order_num_font = win32ui.CreateFont(order_num_font_info)
    dining_font = win32ui.CreateFont(dining_font_info)
    dotted_line_font = win32ui.CreateFont(dotted_line_info)
    font_i1 = win32ui.CreateFont(font_info_i1)
    lf = font_info["height"]
    lf_order_num = order_num_font_info["height"]
    lf_dining = dining_font_info["height"]
    lf_dotted_line = dotted_line_info["height"]

    # extract nested data
    summary = data["summary"]
    payment = list(data["payment"].items())
    payment_data = data["payment"]
    discounted = data["payment"]["discount_applied"]
    discount_amount = data["payment"]["discount_amount"]
    description = data["description"]

    # define store info
    store_info = [store_address, store_phone]
    server_info = ["Time: "+str(datetime.datetime.now())[:-7], \
                "Employee Name: "+description["order_source"]]

    # create print object (PyCDC Object)
    dc = win32ui.CreateDC()
    dc.CreatePrinterDC(printer_name)
    printer_size = dc.GetDeviceCaps(win32con.PHYSICALWIDTH), dc.GetDeviceCaps(win32con.PHYSICALHEIGHT)
    dc.StartDoc("receipt")
    dc.StartPage()

    # Add the store icon
    icon = "icon.png"  #icon path
    bmp = Image.open(icon)
    if bmp.size[0] < bmp.size[1]:
        bmp = bmp.rotate(90)
    image_width, image_height = bmp.size

    bmp = bmp.resize((image_width//4,image_height//4)) 
    image_width, image_height = bmp.size

    linespace+= image_height//2 
    image_position = ((WIDTH - image_width) // 2, linespace)

    dib = ImageWin.Dib(bmp)
    dib.draw(dc.GetHandleOutput(), (image_position[0], image_position[1], 
                                    image_position[0] + image_width, image_position[1] + image_height))
    linespace += image_height

    # Add the default info
    for i, each in enumerate(store_info):
        align_center(each, font)
    # linespace+=lf

    # diffrentiate between cash and card payments
    if description["cash"] == True:
        if description["order_id"] != "":
            order_num = "NOT PAID: " + str(description["order_id"])
            align_center(order_num, order_num_font)
            linespace+=lf
            align_center("Please pay at the cashier to complete your order.", font_i1)
        else:
            align_center("NOT PAID", order_num_font)
            linespace+=lf
            align_center("Please pay at the cashier to complete your order.", font_i1)
    elif description["cash"] == False:
        order_num = str(description["order_id"])
        align_center(order_num, order_num_font)
    
    linespace+=lf
    order_type = "Dining Type: "+str(description["order_type"])
    align_center(order_type, dining_font)
    linespace+=lf

    for i, each in enumerate(server_info):
        align_center(each, font)
    linespace+=lf//2

    summ = 'Summary'
    align_center(summ, font)

    add_line()

    #Add header
    item, price, qty = "ITEM", "PRICE", "QTY"

    align_left(item,font)
    align_right(qty,font)
    sec_align_right(price,font)

    add_line()

    #Add order items
    for k, summary_info in summary.items():
        item_id = summary_info["ID"]
        full_name = summary_info["item"]
        price = summary_info["price"]
        quantity = summary_info["quantity"]
        # size
        size_id = summary_info["size"]
        size = size_id
        if size_id != "":
            size = mod_keys[size_id]
        
        align_left(full_name, font)
        align_right(str(quantity), font)
        sec_align_right("$"+str(price), font)

        if size != "":
            tab_align_left(size, font)

        # show mods for drinks
        if summary_info["category"] == "Drinks":
            # extract drink modifiers
            ice = mod_keys[summary_info["ice"]]
            sugar = mod_keys[summary_info["sugar"]]
            milk = summary_info["milk"]
            if milk != "":
                milk = mod_keys[summary_info["milk"]]
            # toppings_req = summary_info["toppings_required"]
            toppings_add = summary_info["toppings_additional"]
            toppings_default = mod_keys[item_id]
            # format modifiers
            ice_sugar = ice + ", " + sugar
            tab_align_left(ice_sugar, font)
            # # compare default and required toppings list
            # for t in toppings_default:
            #     if t not in toppings_req:
            #         tab_align_left("-" + mod_keys[t], font)
            # add toppings
            # for t in toppings_req:
            #     tab_align_left("+" + mod_keys[t], font)
            for t in toppings_add:
                tab_align_left("+" + mod_keys[t], font)
            tab_align_left(milk, font)
            
        # seperator
        add_line2(dotted_line_font)

    #Add payment
    # linespace+=lf//2
    payment_list = ["subtotal", "discount", "tax", "tip", "total"]
    for i in payment_list:
        if i == "discount":
            if discounted:
                cost = "-$" + str(discount_amount)
            else:
                continue
        elif i == "total":
            total_tip = round(float(payment_data["total"]) + float(payment_data["tip"]), 2)
            cost = "$" + str(total_tip)
        else:
            cost = "$" + str(payment_data[i])
        content = i.capitalize() + ":"
        align_left(content, font) 
        align_right(cost, font)
    linespace+=lf
    add_line()

    # add_qrcode(qr_url)
    linespace+=lf
    #Add footer
    footer = "- Have a wonderful day! -"
    align_center(footer, font)
    linespace+=lf


    dc.EndPage()
    dc.EndDoc()
    dc.DeleteDC()


if __name__ == "__main__":
    f = open("print_order.json")
    data = json.load(f)
    f.close()
    # print receipt
    receipt_width = 580
    print_receipt(receipt_width, data, "POS80")
