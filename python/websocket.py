import websockets
import logging
import asyncio
import json
from print_utils import format_json, print_receipt

async def main_logic(websocket):
    print("**WEBSOCKET LISTEN UP AND RUNNING**")

    # receive data from frontend
    async for message in websocket:
        message_dict = json.loads(message)
        # convert json into python dictionary
        data = format_json(message_dict)

        # print receipt
        receipt_width = 580
        try:
            print_receipt(receipt_width, data, "POS80")
        except Exception as e:
            order_id = data["description"]["order_id"]
            print(order_id, "ERROR ", e)
        

if __name__ == "__main__":
    # websocket server start and run forever
    start_server = websockets.serve(main_logic, "127.0.0.1", 8082)

    logging.info("run websocket listen")
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
