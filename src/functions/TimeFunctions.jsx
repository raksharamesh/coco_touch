// converts 2024-06-01 -> 06/01/2024 format
export function ConvertPythonDate(dateString) {
    const [year, month, day] = dateString.split('-');

    return `${month}/${day}/${year}`;
};

// returns hh:mm (i.e. 15:30)
export function TimeNowSimple() {
    let now = Date.now();
    let timeNow = new Date(now).toLocaleString("en-US", { 
                timeZone: "America/New_York", 
                timeStyle: "short",
                hourCycle: 'h24'
            });

    return timeNow;
};

// returns mm/dd/yyyy (i.e. 05/21/2024)
export function TodaysDateSimple() {
    let now = Date.now();
    let todaysDate = new Date(now).toLocaleString("en-US", { 
                timeZone: "America/New_York", 
                month: "2-digit", 
                day: "2-digit", 
                year: "numeric" 
            });

    return todaysDate;
};

// this does not allow you to define timezone
export function TodaysDate() {
    let now = Date.now();
    let todaysDate = new Date(now).toString();

    return todaysDate;
};

// takes a simple date a returns full datetime string
export function ParseSimpleDate(dateString) {
    const [month, day, year] = dateString.split("/").map(Number);
    return new Date(year, month - 1, day); // Months are 0-indexed in JavaScript
};

// checks if the date falls in set interval returns bool
export function IsDateInInterval(date, dateStart, dateEnd) {
    let currDate = ParseSimpleDate(date);
    let startInterval = ParseSimpleDate(dateStart);
    let endInterval = ParseSimpleDate(dateEnd);

    return (currDate >= startInterval && currDate <= endInterval);
};

// checks if current date is earlier than the end date returns bool
export function IsDateBeforeEnd(date, dateEnd) {
    let currDate = ParseSimpleDate(date);
    let endInterval = ParseSimpleDate(dateEnd);

    return (currDate <= endInterval);
};