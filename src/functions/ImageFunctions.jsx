export function importAllImages (requireContext) {
    return requireContext.keys().map(requireContext);
};
  
export const bannerImages = importAllImages(require.context('../image/banner', false, /\.(png|jpe?g|svg)$/));