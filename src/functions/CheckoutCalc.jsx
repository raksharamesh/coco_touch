// import data
import paymentDevice from '../data/payment_config.json'
const nyTax = paymentDevice["TAX"];

// function to calculate tax
export function calcTax(subtotal) {
    let tax = (Math.round((subtotal * nyTax) * 100) / 100).toFixed(2);
    return tax;
};

// function to calculate total
export function calcTotal(subtotal) {
    let total = (Math.round((subtotal * Number(1 + nyTax)) * 100) / 100).toFixed(2);
    return total;
};

// apply a percentage to subtotal
export function applyPercentage(subtotal, percentage) {
    let newSubtotal = (Math.round((subtotal * percentage) * 100) / 100).toFixed(2);
    return newSubtotal;
};

// pretty round to default 2 dp
export function prettyRounding(number, dp=2) {
    return parseFloat(number).toFixed(dp)
};