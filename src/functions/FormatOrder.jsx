// import data
import drinksData from "../data/drinks.json"
import modsData from "../data/modifiers.json"

// seperate toppings into req and additional + recalculate price
export function FormatDefaultToppings(data) {
    let ordersList = [];

    data.map((i, n) => {
        let cat = i.category;
        let id = i.ID;
        if (cat === "Drinks") {
            let defaultReq = drinksData[id]["Toppings"]["Default"];
            let reqNum = defaultReq.length;
            let toppings = i.toppings;
            // define required and additional toppings list
            let addToppings = [...toppings];
            let reqToppings = [];

            // refactor toppings into required and additional toppings list
            if (toppings.length > 0) {
                // compare toppings in default list and add to required list
                toppings.map((i) => {
                    if (defaultReq.includes(i)) {
                        reqToppings.push(i);
                        addToppings = addToppings.filter(item => item !== i);
                        // update required toppings number
                        if (reqNum > 0) {
                            reqNum -= 1;
                        };
                    };
                });
                // push additional toppings and substitutes if different from default
                if (reqNum > 0) {
                    let remaining = addToppings.splice(0, reqNum);
                    reqToppings = reqToppings.concat(remaining);
                };
            };
            i.toppings_required = reqToppings;
            i.toppings_additional = addToppings;

            // update price
            let newPrice = drinksData[id]["Price"];
            // size price
            let sizeOpt = drinksData[id]["Size"]["Options"];
            let size = i["size"];
            console.log("size", sizeOpt, size, modsData[sizeOpt]["Price"][size]);
            newPrice += modsData[sizeOpt]["Price"][size];
            // milk price
            let milkOpt = drinksData[id]["Milk"]["Options"];
            let milk = i["milk"];
            if (milk) {
                console.log("milk", modsData[milkOpt]["Price"][milk]);
                newPrice += modsData[milkOpt]["Price"][milk];
            };
            // additional toppings price
            let toppingsOpt = drinksData[id]["Toppings"]["Options"];
            addToppings.map((i) => {
                console.log("toppings", modsData[toppingsOpt]["Price"][i]);
                newPrice += modsData[toppingsOpt]["Price"][i];
            });
            i.price = Number(newPrice).toFixed(2);
        };
        ordersList.push(i);
    });

    return ordersList;
};

// push all toppings into the additional toppings category and fill required toppings with default toppings
export function FormatToppings(data) {
    let ordersList = [];

    data.map((i) => {
        let cat = i.category;
        let id = i.ID;
        if (cat === "Drinks") {
            let defaultReq = drinksData[id]["Toppings"]["Default"];
            i.toppings_required = defaultReq;
            i.toppings_additional = i.toppings;
        };
        ordersList.push(i);
    });
    
    return ordersList;
};

// push order items into a dictionary
export function FormatSummary(data) {
    let ordersDict = {};
    data.map((i, n) => {
        ordersDict["Item" + (n + 1)] = i 
    });
    
    return ordersDict;
};