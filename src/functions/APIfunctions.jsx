// import data
import paymentConfig from "../data/payment_config.json"
// import functions
import { CleanPhoneNumber } from "../functions/HelperFunctions";

// stock management config
const MODE = paymentConfig["MODE"]; // test or prod
const backendManagementURL = paymentConfig[MODE]["MANAGEMENT_HOME"];
const backendPaymentURL = paymentConfig[MODE]["APP_HOME"];
// get api params
const storeID = paymentConfig["STORE_NAME"];
const kioskName = paymentConfig["KIOSK_NAME"];
const locationID = paymentConfig[MODE][storeID]["LOCATION_ID"];

export async function GETStock() {
    if (!navigator.onLine) {
        console.log("No internet connection, unable to retrive stock data");
        return;
    };

    // if storeID is undefined do not send request
    if (storeID) {
        const postURL = backendManagementURL + "/kiosks_get_stock?store_number=" + String(storeID) + "&store_chain=coco";

        try {
            const response = await fetch(postURL, {
                method: "GET"
            });
    
            if (response.status === 200) {
                const responseData = await response.json();
                return responseData
            };
            
        } catch (error) {
            console.error("Error fetching stock data: ", error);
            return;
        };
    } else {
        return;
    };
};


export async function GETLock() {
    if (!navigator.onLine) {
        console.log("No internet connection, unable to retrive lock data");
        return;
    };

    // if storeID is undefined do not send request
    if (storeID && kioskName) {
        const postURL = backendManagementURL + "/lock_kiosks?store_number=" + String(storeID) + "&kiosk_name=" + String(kioskName) + "&store_chain=coco";
        
        try {
            const response = await fetch(postURL, {
                method: "GET"
            });
    
            if (response.status === 200) {
                const responseData = await response.json();
                return responseData
            };
            
        } catch (error) {
            console.error("Error fetching lock data: ", error);
            return;
        };
    } else {
        return;
    };
};

export async function GETItemDiscount(item, price) {
    try {
        const response = await fetch(`http://127.0.0.1:5000/api/data/item_discount?item=${item}&price=${price}`);

        if (response.status === 200) {
            const responseData = await response.json();
            return responseData
        } else {
            console.error("Error fetching item discount: ", response.status);
            // return default price if there is an error
            return {"price": price}
        };

    } catch (error) {
        console.error("Error fetching item discount: ", error);
        // return default price if there is an error
        return {"price": price}
    };
};

export async function POSTPhoneNumber(orderID, number) {
    const postURL = backendPaymentURL + "/record_phone_number";
    let phoneNumber = CleanPhoneNumber(number);

    if (phoneNumber) {
        try {
            const response = await fetch(postURL, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    "order_id": orderID,
                    "location_id": locationID,
                    "phone_number": phoneNumber
                })
            });
        
            if (response.status === 200) {
                const responseData = await response.json();
                return responseData
            } else if (response.status === 400) {
                const responseData = await response.json();
                return responseData
            } else {
                return { "status": "Try Again" };
            };
    
        } catch (error) {
            console.error("Error posting phone number: ", error);
            return { "status": "Try Again" };
        };
    } else {
        return { "status": "Invalid US Number." };
    };
    
};