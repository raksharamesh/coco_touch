// make a comprehensive comparison between 2 items
export function DeepEqual(obj1, obj2) {
    if (obj1 === obj2) return true;

    if (typeof obj1 !== 'object' || typeof obj2 !== 'object' || obj1 === null || obj2 === null) {
        return false;
    };

    const keys1 = Object.keys(obj1);
    const keys2 = Object.keys(obj2);

    if (keys1.length !== keys2.length) {
        return false;
    };

    for (const key of keys1) {
        if (!keys2.includes(key) || !DeepEqual(obj1[key], obj2[key])) {
            return false;
        };
    };

    return true;
};

// remove +1 from phone number strinf
export function CleanPhoneNumber(phoneNumber) {
    let cleanedNumber = phoneNumber.replace(/[^\d]/g, '');
    // check if the number starts with 1 (common in US numbers with country code)
    if (cleanedNumber.startsWith('1') && cleanedNumber.length === 11) {
        cleanedNumber = cleanedNumber.substring(1);
    };

    if (cleanedNumber.length === 10) {
        return cleanedNumber;
    } else {
        return false;
    };
}