// function to get unique order ID
// UUID should be <= 32 characters
export default function GenerateUUID() {
    const crypto = window.crypto || window.msCrypto;
    if (crypto && crypto.getRandomValues) {
        const buffer = new Uint8Array(16);
        crypto.getRandomValues(buffer);
        buffer[6] = (buffer[6] & 0x0f) | 0x40; // Version 4
        buffer[8] = (buffer[8] & 0x3f) | 0x80; // Variant RFC4122
        return Array.from(buffer, byte => byte.toString(16).padStart(2, '0')).join('')
    } else {
        // Fallback to a simple random-based implementation if crypto API is not available
        let uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        const r = Math.random() * 16 | 0;
        const v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
        });
        return uuid;
    }
};