import React from "react";
import { motion } from "framer-motion"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import your icons
import { faCartShopping } from '@fortawesome/free-solid-svg-icons'
// import data
import langData from "../../data/vocab.json"

export default function CartTab({ lang, sendToCart, num }) {
    
    return (
        <motion.div className="tabContainer toRight"
            animate={{ x: 0 }}
            initial={{ x: "100%" }}
            transition={{ duration: 0.5 }}
        >
            <div className="tabButtonRight" onClick={() => sendToCart()}>
                <div className="labels">
                    { num > 0 && <p className="labels cartLabelTab">{num}</p> }
                    <FontAwesomeIcon className="buttonText textWhite spacingBot8" icon={faCartShopping} />
                    <p className="textWhite">{langData["Cart"][lang]}</p>
                </div>
            </div>
        </motion.div>
    )
};