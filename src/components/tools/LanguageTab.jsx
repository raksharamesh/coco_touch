import React, { useState, useEffect, useRef } from "react";
import { motion } from "framer-motion";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import your icons
import { faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons'

const langMap = {
    "En": "English",
    "Zh": "中文",
    // "Es": "Español"
}

export default function LanguageTab({ language, sendLang }) {
    const [lang, setLang] = useState(language);
    // flag
    const [showOptions, setShowOptions] = useState(false);
    // get width of vehicleContent div
    const widthRef = useRef(null);
    const [contentWidth, setContentWidth] = useState(0);

    useEffect(() => {
        if (widthRef.current) {
            const width = widthRef.current.getBoundingClientRect().width;
            setContentWidth(width);
        }
    }, [widthRef, language]);

    useEffect(() => {
        setShowOptions(false);
        setLang(language);
    }, [language])


    // language tab
    let tabStyle = showOptions ? "langTab bgOrange" : "langTab";
    let langTab = <div className={tabStyle} onClick={() => setShowOptions(!showOptions)}>
                        <div className="flexParent centerItem">
                            <p className="buttonText textWhite">{language.toUpperCase()}&ensp;</p>
                            <FontAwesomeIcon className="buttonText textWhite spacingBot8" icon={showOptions ? faAngleLeft : faAngleRight} />
                        </div>
                    </div>
    

    // create language options
    let langOptions = Object.keys(langMap).map((i) => {
        let label = langMap[i];
        let style = lang === i ? "buttonText selectedText textLeft" : "buttonText textMedGrey textLeft";
        return (
            <div className="langUnit" onClick={() => sendLang(i)}>
                <p className={style}>{label}</p>
            </div>
        )
    });

    let langDrawer = <div className="langDrawer" ref={widthRef}>
                            { langOptions }
                        </div>


    return (
        <div className="langContainer toLeft">
            <motion.div className="flexParent"
                initial={{ x: showOptions ? -contentWidth : 0 }}
                animate={{ x: showOptions ? 0 : -contentWidth }}
                transition={{ duration: 0.4 }}
            >
                { langDrawer }
                { langTab }
            </motion.div>
        </div>
    )
};