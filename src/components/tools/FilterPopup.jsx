import React, { useState, useEffect } from "react";
import { motion } from "framer-motion"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import your icons
import { faLeaf, faCubesStacked, faCoffee, faCow, faBan, faCircleXmark } from '@fortawesome/free-solid-svg-icons'

// create list for all icons
const filterIcons = [
    { index: 0, name: faLeaf, label: "Vegan Friendly", id: "Vegan", ban: false },
    { index: 1, name: faCubesStacked, label: "Sugar Free",  id: "noSugar", ban: true },
    { index: 2, name: faCoffee, label: "Caffeine Free",  id: "noCaffeine", ban: true },
    { index: 3, name: faCow, label: "Dairy Free",  id: "noDairy", ban: true }
];

export default function FilterPopup({ filterDrinks, sendFilter, sendOpen }) {
    const [filter, setFilter] = useState([]);

    // set filter sent from parent everytime component mounts
    useEffect(() => {
        filterDrinks.map((i) => {
            toogleFilter(i)
        });
    }, [sendOpen]);

    // send to parent
    useEffect(() => {
        sendFilter(filter);
    }, [filter]);


    ///////////////
    // FUNCTIONS //
    ///////////////
    // iterate through all the children in a container and remove the identified class
    function removeAllClass(containerName, styleName) {
        // Remove the class if it exists
        if (containerName.classList.contains(styleName)) {
            containerName.classList.remove(styleName);
        };
    
        // Recursively traverse through child elements
        if (containerName.children.length > 0) {
            for (let i = 0; i < containerName.children.length; i++) {
                removeAllClass(containerName.children[i], styleName);
            }
        };
    };

    // add class
    function addClass(id, styleName) {
        try {
            let element = document.getElementById(id);
            element.classList.add(styleName);
        } catch (error) {
            console.log("ADD CLASS: ", error);
        };
    };

    // function to toggle filtered selection
    function toogleFilter(id) {
        // remove filter if already selected
        if (filter.includes(id)) {
            setFilter(list => list.filter(item => item !== id));
            // remove all styling
            let filterContainer = document.getElementById("filter");
            removeAllClass(filterContainer, "selectedIconText");
            removeAllClass(filterContainer, "selectedFilter");

        } else {
            setFilter([id]);
            // remove all styling
            let filterContainer = document.getElementById("filter");
            removeAllClass(filterContainer, "selectedIconText");
            removeAllClass(filterContainer, "selectedFilter");

            // add styling to selected filter
            addClass(id, "selectedIconText");
            addClass(id, "selectedFilter");
            let filterElem = document.getElementById(id).children;
            for (let i = 0; i < filterElem.length; i++) {
                filterElem[i].classList.add("selectedIconText");
            };
        };

    };

    /////////////////////////
    // CREATE FILTER LIST //
    ////////////////////////
    // generate divs iteratively through list
    let filterList = filterIcons.map(i => {
        let id = i.id;
        let name = i.name;
        let label = i.label;
        let ban = i.ban;
        
        return (
            <div id={id} className="flexParent filterUnit" onClick={() => toogleFilter(id)}>
                <FontAwesomeIcon className="filterIcon" icon={name} size="3x" />
                { ban && <FontAwesomeIcon className="banIcon" icon={faBan} size="4x" /> }
                <p className="filterText text">{label}</p>
            </div>
        )
    });


    return (
        <div className="popupVeil">
            <motion.div id="filter" className="popupContainer"
                initial={{ left: "-50vw", }} 
                animate={{ left: "50%", }} 
                transition={{ duration: 0.2 }}
            >
                <div className="exitContainer" onClick={() => sendOpen(false)}>
                    <FontAwesomeIcon className="textGrey" icon={faCircleXmark} size="3x" />
                </div>
                <div>
                    <p className="title spacingBig spacingTop5">Filter Drinks</p>
                </div>
                { filterList }
            </motion.div>
        </div>
    )
};

