import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import your icons
import { faLeaf, faBottleDroplet, faCoffee, faCow, faBan, faCircleXmark } from '@fortawesome/free-solid-svg-icons'
// import data
import langData from "../../data/vocab.json"

// create list for all icons
const filterIcons = [
    { index: 0, name: faLeaf, id: "Vegan", ban: false },
    { index: 1, name: faBottleDroplet, id: "noLactose", ban: true },
    { index: 2, name: faCoffee, id: "noCaffeine", ban: true },
    { index: 3, name: faCow, id: "noDairy", ban: true }
];

export default function FilterList({ language, sendFilter }) {
    const [filter, setFilter] = useState([]);
    const [lang, setLang] = useState(language);

    useEffect(() => {
        setLang(language);
    }, [language]);

    // send to parent
    useEffect(() => {
        sendFilter(filter);
    }, [filter]);

    ///////////////
    // FUNCTIONS //
    ///////////////
    // iterate through all the children in a container and remove the identified class
    function removeAllClass(containerName, styleName) {
        // Remove the class if it exists
        if (containerName.classList.contains(styleName)) {
            containerName.classList.remove(styleName);
        };
    
        // Recursively traverse through child elements
        if (containerName.children.length > 0) {
            for (let i = 0; i < containerName.children.length; i++) {
                removeAllClass(containerName.children[i], styleName);
            }
        };
    };

    // add class
    function addClass(id, styleName) {
        try {
            let element = document.getElementById(id);
            element.classList.add(styleName);
        } catch (error) {
            console.log("ADD CLASS: ", error);
        };
    };

    // function to toggle filtered selection
    function toogleFilter(id) {
        // remove filter if already selected
        if (filter.includes(id)) {
            setFilter(list => list.filter(item => item !== id));
            // remove all styling
            let filterContainer = document.getElementById("filter");
            removeAllClass(filterContainer, "textOrange");
            removeAllClass(filterContainer, "buttonSelected");

        } else {
            setFilter([id]);
            // remove all styling
            let filterContainer = document.getElementById("filter");
            removeAllClass(filterContainer, "textOrange");
            removeAllClass(filterContainer, "buttonSelected");

            // add styling to selected filter
            addClass(id, "textOrange");
            addClass(id, "buttonSelected");
            let filterElem = document.getElementById(id).children;
            for (let i = 0; i < filterElem.length; i++) {
                filterElem[i].classList.add("textOrange");
            };
        };

    };

    /////////////////////////
    // CREATE FILTER LIST //
    ////////////////////////
    // generate divs iteratively through list
    let filterList = filterIcons.map(i => {
        let id = i.id;
        let name = i.name;
        let label = langData[id][lang];
        let ban = i.ban;
        
        return (
            <div className="flexAuto centerItem">
                <button id={id} className="buttonBlank buttonFilter flexParent" onClick={() => toogleFilter(id)}>
                    <div className="centerItem">
                        { ban && <FontAwesomeIcon className="text banIconSmall" icon={faBan} /> }
                        <FontAwesomeIcon className="text" icon={name} />
                    </div>
                    <p className="labels">&emsp;{label}</p>
                </button>
            </div>
        )
    });


    return (
        <div id="filter" className="flexParent">
            { filterList }
        </div>
    )
};