import React, { useEffect, useState } from 'react';
// import functions
import { bannerImages } from '../../functions/ImageFunctions';

export default function ImageSlideShow({ }) {
    // set the images to be iterated through
    const [images, setImages] = useState([]);
    const [currentImageIndex, setCurrentImageIndex] = useState(0);

    useEffect(() => {
        setImages(bannerImages);
    }, []);
    
    useEffect(() => {
        // start slideshow when component mounts
        const slideshowInterval = setInterval(() => {
            setCurrentImageIndex((prevIndex) => (prevIndex + 1) % images.length);
        }, 10000); // 10 secs interval

        return () => {
            // clear interval when component unmounts
            clearInterval(slideshowInterval);
        };
    }, [images]);
    
    return (
        <div className="adSpace">
            <img className="adImage" src={images[currentImageIndex]} alt={`Image ${currentImageIndex}`} />
        </div>
    );

};