import React, { useState, useEffect } from "react";
import { motion } from "framer-motion"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import your icons
import { faCartShopping } from '@fortawesome/free-solid-svg-icons'
// import data
import langData from "../../data/vocab.json"

export default function CheckoutFooter({ language, cartFill, sendState }) {
    const [lang, setLang] = useState(language);

    useEffect(() => {
        setLang(language);
    }, [language]);

    let button = <motion.div className="footerContainer centerItem"
                    initial={{ y: "100%", }} 
                    animate={{ 
                        y: "0%", 
                        transition: { type: "spring", duration: 1, stiffness: 100 }
                    }} 
                 >
                    <button className="buttonBlank buttonCheckout centerItem" onClick={() => sendState("checkout")}>
                        <p className="buttonSubtitle textWhite">
                            <span>{langData["Cart"][lang]}&ensp;</span>
                            <FontAwesomeIcon icon={faCartShopping} />
                        </p>
                    </button>
                </motion.div>

    return (
        <>
            { cartFill && button }
        </>
    )
};