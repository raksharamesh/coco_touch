import React from 'react';

export default function PythonWebsocket({ print }) {
    const [socket, setSocket] = React.useState(null);

    // connect to Python server
    React.useEffect(() => {
        // Establish Websocket connection to the Python server
        const ws = new WebSocket('ws://localhost:8082/');

        ws.onopen = () => {
            console.log('WebSocket connected!');
            setSocket(ws);
        };

        ws.onmessage = (event) => {
            const receivedMessage = event.data;
            console.log(`Received: ${receivedMessage}`);
        };

        ws.onclose = () => {
            console.log('WebSocket disconnected!');
        };

        return () => {
            // Clean up the WebSocket connection when component unmounts
            if (socket) {
                socket.close();
            }
        };

    }, []);

    // set message to send to Python server
    React.useEffect(() => {
        if (print !== undefined) {
            console.log("Print Order: ", print);
            socket.send(print);
        };
    }, [print]);
};