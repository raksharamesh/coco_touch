import React from "react";
import { motion } from "framer-motion"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import your icons
import { faCircleLeft } from '@fortawesome/free-solid-svg-icons'
// import data
import langData from "../../data/vocab.json"

export default function BackTab({ lang, sendBack }) {
    return (
        <motion.div className="tabContainer toLeft"
            animate={{ x: 0 }}
            initial={{ x: "-100%" }}
            transition={{ duration: 0.5 }}
        >
            <div className="tabButtonLeft" onClick={() => sendBack()}>
                <div className="labels">
                    <FontAwesomeIcon className="buttonText textWhite spacingBot8" icon={faCircleLeft} />
                    <p className="textWhite">{langData["Back"][lang]}</p>
                </div>
            </div>
        </motion.div>
    )
};
