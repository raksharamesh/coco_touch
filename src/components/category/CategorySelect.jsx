import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import your icons
import { faFilter } from '@fortawesome/free-solid-svg-icons'
// import image
import saleImg from "../../image/Sale Sticker.png"
// import data
import catData from "../../data/category.json"
import drinkData from "../../data/menu.json"
import itemsData from "../../data/itemsAll.json"
import defaultStockData from "../../data/stock.json"
import paymentConfig from "../../data/payment_config.json"
import langData from "../../data/vocab.json"
// import functions
import { GETStock } from "../../functions/APIfunctions";
import { prettyRounding } from "../../functions/CheckoutCalc";

// get store id
const storeID = paymentConfig["STORE_NAME"];

export default function CategorySelect({ language, category, title, filterData, sendState, sendCat, sendItem, sendOpen }) {
    const [lang, setLang] = useState(language);
    const [nameLang, setNameLang] = useState("Name " + language);
    const [selCat, setSelCat] = useState("");
    const [filter, setFilter] = useState(filterData);
    const [stockData, setStockData] = useState(defaultStockData); // default all items in stock
    // flag
    const [noDrinks, setNoDrinks] = useState(false);
    // initialize local data store
    let numberOfDrinks;
    let catDrinks = {};

    useEffect(() => {
        setLang(language);
        setNameLang("Name " + language);
    }, [language]);

    // call stock API
    useEffect(() => {
        async function callAPI() {
            const newData = await GETStock();
            if (newData) {
                setStockData(newData);
            } else {
                // data is undefined
                setStockData(defaultStockData);
            };
        };
        callAPI();
    }, [category, filterData])

    useEffect(() => {
        setFilter(filterData);
    }, [filterData]);

    useEffect(() => {
        // reset popup
        setNoDrinks(false);
        if (category !== "") {
            setSelCat(category);
            sendCat(category);
        };
    }, [category]);

    // determine when to show popup
    useEffect(() => {
        if (numberOfDrinks <= 0) {
            setNoDrinks(true);
        } else {
            setNoDrinks(false);
        };
    }, [selCat, filterData, catDrinks]);

    ////////////////
    // FUNCTIONS //
    ///////////////
    function selectCategory(id) {
        setSelCat(id);
        // send data to parent
        sendState("pickDrink");
        sendCat(id);
    };

    function selectDrink(id) {
        // send to parent
        if (itemsData[id]["Main Category"] === "Drinks") {
            sendState("drinkFlow");
        } else {
            sendState("itemFlow")
        }
        sendItem(id);
    };

    // filter drink data according to selected filter value
    function filterDrinks(storeData) {
        filter.map((f) => {
            Object.keys(drinkData[selCat]).map((k) => {
                if (drinkData[selCat][k][f]) {
                    storeData[k] = drinkData[selCat][k];
                };
            });
        })
    };

    // function to open filter popup
    function clickFilter() {
        sendOpen(true);
        setNoDrinks(false);
    };


    //////////////////////
    // CREATE STICKERS //
    /////////////////////
    let soldoutSticker = <div className="soldoutStickerCat">
                            <p className="labelsSmall bold textWhite">{langData["Sold Out"][lang]}</p>
                        </div>

    let saleSticker = <div className="posRelative">
                            <img className="img25 toRight" src={saleImg} />
                        </div>
    


    /////////////////////////////
    // CREATE NO DRINKS POPUP //
    ////////////////////////////
    let noItemsText = langData["No Drinks"][lang].split('|');
    const noItemsPopup = 
        <div className="noDrinkPopUp">
            <p className="text spacing selectedText">
                <span>{noItemsText[0]}</span>
                <span>{title}</span>
                <span>{noItemsText[1]}</span>
            </p>
            {/* <div className="centerItem">
                <button className="buttonBlank buttonOutlineWhite bgOrange" onClick={() => clickFilter()}>
                    <p className="labels textWhite">
                        <span>Filter&ensp;</span>
                        <span><FontAwesomeIcon icon={faFilter} /></span>
                    </p>
                </button>
            </div> */}
        </div>


    ////////////////////////////
    // CREATE DRINKS DISPLAY //
    ///////////////////////////
    // show all drink categories
    let catItems;

    // show drinks from selected category
    if (Object.keys(catData).includes(selCat)) {
        // filter drinks
        if (filter.length > 0) {
            filterDrinks(catDrinks);
            numberOfDrinks = Object.keys(catDrinks).length;
        } else {
            catDrinks = drinkData[selCat];
            numberOfDrinks = Object.keys(catDrinks).length;
        };

        // generate drink images iteratively through the filtered array
        catItems = Object.keys(catDrinks).map((k) => {
            let image = catDrinks[k]["Image"];
            let name = catDrinks[k][nameLang];
            let price = prettyRounding(catDrinks[k]["Price"]);
            let originalPrice = prettyRounding(catDrinks[k]["Original Price"]);
            let discounted = catDrinks[k]["Discounted"];

            let newOriginalPrice = <p className="text drinkText">
                                    <span className="textStrike textGrey">${originalPrice}</span>
                                    <span> &emsp; ${price}</span>
                                </p>

            // only allow click if in stock
            if (stockData[k]) {
                return (
                    <div id={k} onClick={() => selectDrink(k)}>
                        { discounted && saleSticker }
                        <img className="catImage" src={require('../../image/drinks/' + image)} />
                        <p className="title drinkTitle spacing">{name}</p>
                        { discounted ? newOriginalPrice : <p className="text drinkText">${price}</p> }
                    </div>
                )
            } else {
                return (
                    <div id={k} className="posRelative centerItem">
                        { soldoutSticker }
                        <div className="imageSoldOut">
                            { discounted && saleSticker }
                            <img className="catImage" src={require('../../image/drinks/' + image)} />
                            <p className="title drinkTitle spacing">{name}</p>
                            { discounted ? newOriginalPrice : <p className="text drinkText">${price}</p> }
                        </div>
                    </div>
                )
            };
            
        });
    } else {
        catItems = Object.keys(catData).map((id) => { 
            let image = catData[id]["Image"];
            return (
                <div id={id} onClick={() => selectCategory(id)}>
                    <img className="catImage" src={require('../../image/category/' + image)} />
                </div>
            )
        });
    }


    return (
        <>
            { noDrinks ? noItemsPopup : null }
            <div className="catGrid">
                { catItems }
            </div>
        </>
    )
};