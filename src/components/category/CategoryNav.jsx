import React, { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import your icons
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
// import data
import catData from "../../data/category.json"

export default function CategoryNav({ language, category, sendCat }) {
    const [lang, setLang] = useState(language);
    const [nameLang, setNameLang] = useState("Name " + language);
    const [cat, setCat] = useState(category);

    useEffect(() => {
        setLang(language);
        setNameLang("Name " + language);
    }, [language]);

    useEffect(() => {
        setCat(category);
    }, [category]);

    // generate divs iteratively through drink category json
    let catHorizontal = Object.keys(catData).map((id, i) => {
        let name = catData[id][nameLang];
        let image = catData[id]["Image"];
        let buttonStyle = id == cat ? "buttonBlank buttonCat centerItem bgRed buttonGlow" : "buttonBlank centerItem buttonCat";
        let textStyle = id == cat ? "subtitleSmall textWhite" : "subtitleSmall";

        return (
            <motion.div id={id} className="catScrollContainer" onClick={() => sendCat(id)}
                animate={{ 
                    x: 0,
                    opacity: 1,
                    // filter: "blur(0px)",
                    transition: { type: "spring", duration: 0.8,  delay: i * 0.075 }
                }}
                initial={{ 
                    x: "-500%",
                    opacity: 0,
                    // filter: "blur(5px)",
                 }}
            >
                <button className={buttonStyle}>
                    <p className={textStyle}>{name}</p>
                </button>
                {/* <img className="catScrollImage" src={require("../../image/category/" + image)} /> */}
            </motion.div>
        )
    });

    return (
        <div className="horizontalScrollContainer">
            <div className="catNavTab centerItem">
                <FontAwesomeIcon className="subtitleSmall textWhite" icon={faChevronRight} />
            </div>
            <div className="catNavContainer" style={{overflowX: "scroll"}}> 
                { catHorizontal }
            </div>
        </div>
    )
};