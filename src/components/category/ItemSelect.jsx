import React, { useState, useEffect } from "react";
// import image
import saleImg from "../../image/Sale Sticker.png"
// import data
import itemsData from "../../data/itemsAll.json"
import defaultStockData from "../../data/stock.json"
import langData from "../../data/vocab.json"
// import functions
import { GETStock } from "../../functions/APIfunctions";
import { prettyRounding } from "../../functions/CheckoutCalc";

export default function ItemSelect({ language, filterData, sendState, sendItem }) {
    const [lang, setLang] = useState(language);
    const [nameLang, setNameLang] = useState("Name " + language);
    const [filter, setFilter] = useState(filterData);
    const [stockData, setStockData] = useState(defaultStockData); // default all items in stock
    const [displayData, setDisplayData] = useState(itemsData);
    const [numberOfDrinks, setNumberOfDrinks] = useState(1);
    // flag
    const [noDrinks, setNoDrinks] = useState(false);

    useEffect(() => {
        setLang(language);
        setNameLang("Name " + language);
    }, [language]);

    useEffect(() => {
        setFilter(filterData);
        callStockAPI();
    }, [filterData]);

    // set data
    useEffect(() => {
        if (filter.length > 0) {
            setDisplayData(filterDrinks(itemsData, filter, true));
        } else {
            setDisplayData(itemsData);
        };
    }, [filter, language]);

    useEffect(() => {
        setNumberOfDrinks(Object.keys(displayData).length);
    }, [displayData])

    // determine when to show popup
    useEffect(() => {
        if (numberOfDrinks <= 0) {
            setNoDrinks(true);
        } else {
            setNoDrinks(false);
        };
    }, [numberOfDrinks]);


    ////////////////
    // FUNCTIONS //
    ///////////////
    async function callStockAPI() {
        const newData = await GETStock();
        if (newData) {
            setStockData(newData);
        } else {
            // data is undefined
            setStockData(defaultStockData);
        };
    };

    function selectDrink(id) {
        sendState("drinkFlow");
        sendItem(id);
    };

    // filter drink data according to selected filter value
    function filterDrinks(items, key, value) {
        const filteredItems = {};
        Object.keys(items).forEach((itemKey) => {
            if (items[itemKey][key] === value) {
                filteredItems[itemKey] = items[itemKey];
            };
        });
        return filteredItems;
    };

    //////////////////////
    // CREATE STICKERS //
    /////////////////////
    let soldoutSticker = <div className="soldoutStickerCat">
                            <p className="labelsSmall bold textWhite">{langData["Sold Out"][lang]}</p>
                        </div>

    let saleSticker = <div className="posRelative">
                            <img className="img25 toRight" src={saleImg} />
                        </div>


    /////////////////////
    // DRINKS DISPLAY //
    ////////////////////
    let catItems = Object.keys(displayData).map((k) => {
        let image = displayData[k]["Image"];
        let name = displayData[k][nameLang];
        let price = prettyRounding(displayData[k]["Price"]);
        let originalPrice = prettyRounding(displayData[k]["Original Price"]);
        let discounted = displayData[k]["Discounted"];

        let newOriginalPrice = <p className="text drinkText">
                                <span className="textStrike textGrey">${originalPrice}</span>
                                <span> &emsp; ${price}</span>
                            </p>

        // only allow click if in stock
        if (stockData[k]) {
            return (
                <div id={k} onClick={() => selectDrink(k)}>
                    { discounted && saleSticker }
                    <img className="catImage" src={require('../../image/drinks/' + image)} />
                    <p className="title drinkTitle spacing">{name}</p>
                    { discounted ? newOriginalPrice : <p className="text drinkText">${price}</p> }
                </div>
            )
        } else {
            return (
                <div id={k} className="posRelative centerItem">
                    { soldoutSticker }
                    <div className="imageSoldOut">
                        { discounted && saleSticker }
                        <img className="catImage" src={require('../../image/drinks/' + image)} />
                        <p className="title drinkTitle spacing">{name}</p>
                        { discounted ? newOriginalPrice : <p className="text drinkText">${price}</p> }
                    </div>
                </div>
            )
        };
        
    });


    //////////////////////
    // NO DRINKS POPUP //
    /////////////////////
    let noItemsText = langData["No Drinks"][lang].split('|');
    const noItemsPopup = 
        <div className="noDrinkPopUp">
            <p className="text spacing selectedText">
                <span>{noItemsText[0]}</span>
                <span>{noItemsText[1]}</span>
            </p>
        </div>


    return (
        <>
            { noDrinks ? noItemsPopup : null }
            <div className="catGrid">
                { catItems }
            </div>
        </>
    )
};