import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import your icons
import { faHouse, faFilter, faCartShopping } from '@fortawesome/free-solid-svg-icons'
// import components
import FilterList from "../tools/FilterList";
// import data
import paymentData from "../../data/payment_config.json";

// get device type
const DEVICE = paymentData["DEVICE_TYPE"];

export default function Header ({ language, title, cartNum, isHome, sendState, sendFilter, sendOpen, sendHome }) {
    const [lang, setLang] = useState(language);
    const [cartFill, setCartFill] = useState(false);
    const [filter, setFilter] = useState([]);

    useEffect(() => {
        setLang(lang);
    }, [language]);

    useEffect(() => {
        if (cartNum >= 1) {
            setCartFill(true);
        } else {
            setCartFill(false);
        };
    }, [cartNum]);

    useEffect(() => {
        sendFilter(filter);
    }, [filter])

    ///////////////////
    // CREATE ICONS //
    //////////////////
    let homeIcon = <div className="headerIcons">
                        { DEVICE !== "CART" && <FontAwesomeIcon className="icons" icon={faHouse} size="3x" onClick={() => sendHome(true)} /> }
                    </div>


    let cartIconStyle = cartFill ? "selectedIconText icons" : "icons";
    let cart = <div className="headerIcons" onClick={() => sendState("checkout")}>
                    <FontAwesomeIcon className={cartIconStyle} icon={faCartShopping} size="3x"/>
                    { cartFill ? <p className="labels cartLabel">{cartNum}</p> : <></> }
                </div>

    // create filter button
    let filterButton = <div className="centerItem" onClick={() => sendOpen(true)}>
                    <button className="buttonBlank buttonOutlineWhite">
                        <p className="labels textOrange">
                            <span>Filter&ensp;</span>
                            <span><FontAwesomeIcon icon={faFilter} /></span>
                        </p>
                    </button>
                </div>


    return (
        <div className="headerContainer flexParent">
            { homeIcon }          
            <div className="headerText">
                <p className="title selectedTitle spacingMini">{title}</p>
                { !isHome && <FilterList language={language} sendFilter={setFilter} /> }
                {/* { filterButton } */}
            </div>
            { cart }
        </div>
    );
};

