import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import your icons
import { faCircleXmark, faCirclePlus, faCircleMinus } from '@fortawesome/free-solid-svg-icons'
// import components
import BackTab from "../tools/BackTab";
import CartTab from "../tools/CartTab";
// import data
import itemsData from "../../data/itemsAll.json";
import langData from "../../data/vocab.json"


export default function ItemPopup({ language, item, cartNum, sendState, sendOrder }) {
    const [lang, setLang] = useState(language);
    const [nameLang, setNameLang] = useState("Name " + lang);
    const [quantity, setQuantity] = useState(1);
    const [price, setPrice] = useState(itemsData[item]["Price"]);
    const [currOrder, setCurrOrder] = useState({
        "ID": item,
        "category": itemsData[item]["Main Category"],
        "quantity": quantity,
        "item": itemsData[item]["Name En"],
        "size": "",
        "ice": "",
        "sugar": "",
        "toppings": [],
        "toppings_required": [],
        "toppings_additional": [],
        "milk": "",
        "price": itemsData[item]["Price"]
    });

    useEffect(() => {
        setLang(language);
        setNameLang("Name " + language);
    }, [language]);

    useEffect(() => {
        let itemPrice = itemsData[item]["Price"] * quantity;
        if (itemPrice > 0) {
            setPrice("$" + itemPrice);
        } else {
            setPrice("");
        };
    }, [item, quantity])

    ////////////////
    // FUNCTIONS //
    ///////////////
    function cancel() {
        sendState("");
    };

    function goToCart() {
        sendState("checkout")
    };

    function addToCart() {
        sendState("confirmOrder");
        sendOrder(currOrder);
    };

    function add1() {
        setQuantity(quantity + 1);
        setCurrOrder(currOrder => ({...currOrder, 'quantity': quantity + 1}));
    };

    function remove1() {
        if (quantity > 0) {
            setQuantity(quantity - 1);
            setCurrOrder(currOrder => ({...currOrder, 'quantity': quantity - 1}));
        };
    };

    /////////////////////////////
    // CREATE QUANTITY SELECT //
    ////////////////////////////
    let quantitySelector = <div className="div40 quantContainer centerItem flexParent spacing">
                                <div className="floatLeft">
                                    <button className="buttonBlank" onClick={() => remove1()}>
                                        <FontAwesomeIcon className="buttonText textMedGrey" icon={faCircleMinus} />
                                    </button>
                                </div>
                                <div className="flex1">
                                    <p className="buttonSubtitle textMedGrey">{quantity}</p>
                                </div>
                                <div className="floatRight">
                                    <button className="buttonBlank" onClick={() => add1()}>
                                        <FontAwesomeIcon className="buttonText textMedGrey textRight" icon={faCirclePlus} />
                                    </button>
                                </div>
                            </div>

    /////////////////////
    // CREATE BUTTONS //
    ////////////////////
    let cancelButton = <button className="buttonBlank buttonSubmitPopup centerItem" onClick={() => cancel()}>
                            <p className="buttonSubtitle textWhite">{langData["Cancel"][lang]}</p>
                        </button>

    let addButton = <button className="buttonBlank buttonSubmitPopup centerItem bgRed" onClick={() => addToCart()}>
                        <p className="buttonSubtitle textWhite">{langData["Add"][lang]}</p>
                    </button>


    return (
        <div className="popupVeil">
            <BackTab lang={lang} sendBack={cancel} />
            <CartTab lang={lang} sendToCart={goToCart} num={cartNum} />
            <div className="popupContainer">
                <div className="exitContainer" onClick={() => cancel()} >
                    <FontAwesomeIcon className="textGrey buttonSubtitle" icon={faCircleXmark} />
                </div>
                <div className="spacingTop5 spacing">
                    <p className="middletitle selectedTitle">{itemsData[item][nameLang]}</p>
                    <p className="subtitle spacing">{price}</p>
                    <img className="drinkImage centerItem" src={require('../../image/drinks/' + itemsData[item]["Image"])} alt=""/>
                </div>
                { quantitySelector }
                <div className="flexAuto centerItem">
                    { cancelButton }
                    { addButton }
                </div>
            </div>
        </div>
    )
};