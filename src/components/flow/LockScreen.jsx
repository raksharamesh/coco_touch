import React from "react";
// import images
import logo from "../../image/logo.png"

export default function LockScreen() {
    return (
        <div className="lockContainer">
            <div className="centerItem spacingBig">
                <img className="img25" src={logo} alt="logo"/>
            </div>
            <div className="div80 spacingTop5">
                <p className="title selectedText">Apologies, we are not accepting orders at the moment.</p>
            </div>
        </div>
    )
}