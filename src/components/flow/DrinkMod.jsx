import React, { useState, useEffect } from "react";
import { motion } from "framer-motion"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import your icons
import { faCircleLeft, faCartShopping, faCircleInfo, faCirclePlus, faCircleXmark } from '@fortawesome/free-solid-svg-icons'
// import component
import BackTab from "../tools/BackTab";
import CartTab from "../tools/CartTab";
// import data 
import drinksData from '../../data/drinks.json'
import modsData from '../../data/modifiers.json'
import modKeysData from '../../data/modKeys.json'
import defaultStockData from '../../data/stock.json'
import paymentConfig from '../../data/payment_config.json'
import langData from "../../data/vocab.json"
// import functions
import { GETStock, GETItemDiscount } from "../../functions/APIfunctions";
import { prettyRounding } from "../../functions/CheckoutCalc";
// import images
import saleImg from "../../image/Sale Sticker.png"
import drinkHotImg from '../../image/order/Hot.png'
import drinkIcedImg from '../../image/order/Ice.png'
import sugarImg from '../../image/order/Sugar.png'

// NOTE: 
// UNCOMMENT toppingsPre AND toppingsPreNum FOR DEFUALT TOPPINGS TO BE REINSTATED

const storeID = paymentConfig["STORE_NAME"];
const baseAddPrice = 0.69;

export default function DrinkMod({ sendOrder, language, getDrink, cartDisplay, cartNum, isEdit, editOrder, sendState, sendIsEdit }) {
    const [lang, setLang] = useState(language);
    const [nameLang, setNameLang] = useState("Name " + lang);
    const [stockData, setStockData] = useState(defaultStockData); // default all items in stock
    const [describeLang, setDescribeLang] = useState("Description " + lang);
    const [displayName, setDisplayName] = useState("");
    const [displayPrice, setDisplayPrice] = useState("");
    const [displayOldPrice, setDisplayOldPrice] = useState("");
    const [displaySize, setDisplaySize] = useState("");
    const [displayIce, setDisplayIce] = useState("");
    const [displaySugar, setDisplaySugar] = useState("");
    const [milkSub, setMilkSub] = useState("");
    const [toppingsSub, setToppingsSub] = useState("");
    // user selection
    const [currCalories, setCurrCalories] = useState("");
    const [currPrice, setCurrPrice] = useState("");
    const [currSize, setCurrSize] = useState(""); 
    const [currIce, setCurrIce] = useState(""); 
    const [currSugar, setCurrSugar] = useState(""); 
    const [currToppings, setCurrToppings] = useState([]);
    const [currMilk, setCurrMilk] = useState("");
    const [maxToppings, setMaxToppings] = useState(0);
    // const [chooseToppings, setChooseToppings] = useState(0);
    // flag
    const [onSale, setOnSale] = useState(false);
    const [getInfo, setGetInfo] = useState(false);
    const [fixedSize, setFixedSize] = useState(false);
    const [fixedIce, setFixedIce] = useState(false);
    const [fixedSugar, setFixedSugar] = useState(false);
    // const [rmToppings, setRmToppings] = useState(false);
    const [milkOpt, setMilkOpt] = useState(false);
    const [warning, setWarning] = useState(false);
    // local data
    const [size, setSize] = useState([]);
    const [ice, setIce] = useState([]);
    const [sugar, setSugar] = useState([]);
    const [toppings, setToppings] = useState([]);
    const [milk, setMilk] = useState([]);
    const [milkDefault, setMilkDefault] = useState(""); // special case: if default milk doesn't incur additional costs
    // const [toppingsPre, setToppingsPre] = useState([]);
    // const [toppingsPreNum, setToppingsPreNum] = useState(0);
    // define set price
    const [milkPrice, setMilkPrice] = useState(0);
    // store all current drink data here
    const [currOrder, setCurrOrder] = useState({
        "ID": getDrink,
        "category": "Drinks",
        "quantity": 1,
        "item": drinksData[getDrink]["Name En"],
        "size": currSize,
        "ice": currIce,
        "sugar": currSugar,
        "toppings": [currToppings],
        "toppings_required": [],
        "toppings_additional": [],
        "milk": "",
        "price": currPrice
    });

    console.log("EDIT ORDER ", isEdit, editOrder, currMilk);

    ////////////////////
    // API FUNCTIONS //
    ///////////////////
    async function discountAPI(price) {
        console.log("PRICE ", currPrice)
        try {
            const data = await GETItemDiscount(getDrink, price);
            setCurrOrder(currOrder => ({...currOrder, "price": data.price}));
            setDisplayPrice("$" + prettyRounding(data.price));
        } catch (error) {
            console.error('Error fetching discount:', error);
            setDisplayPrice("$" + prettyRounding(currPrice));
        };
    };

    async function stockAPI() {
        const newData = await GETStock();
        if (newData) {
            setStockData(newData);
        } else {
            // data is undefined
            setStockData(defaultStockData);
        };
    };


    // call stock API
    useEffect(() => {
        stockAPI();
    }, []);

    useEffect(() => {
        setLang(language);
        setNameLang("Name " + language);
    }, [language]);

    // extract data
    useEffect(() => {
        let itemData = drinksData[getDrink];
        setDisplayName(itemData[nameLang]);
        setOnSale(itemData["Discounted"]);
        setDisplayOldPrice(itemData["Original Price"])
        // set calories
        let minCal = itemData["Calories"]["Min"];
        let maxCal = itemData["Calories"]["Max"];
        let caloriesText = langData["Calories"][lang];
        let calText = minCal === maxCal ? `${minCal} ${caloriesText}` : `${minCal}-${maxCal} ${caloriesText}`;
        setCurrCalories(calText);
        // set size data
        let itemSize = itemData["Size"];
        setSize(modsData[itemSize["Options"]]["Options"]);
        setFixedSize(modsData[itemSize["Options"]]["Fixed"]);
        setCurrSize(itemSize["Default"]);
        // set ice data
        let itemIce = itemData["Ice"];
        setIce(modsData[itemIce["Options"]]["Options"]);
        setFixedIce(modsData[itemIce["Options"]]["Fixed"]);
        setCurrIce(itemIce["Default"]);
        // set sugar data
        let itemSugar = itemData["Sugar"];
        setSugar(modsData[itemSugar["Options"]]["Options"]);
        setFixedSugar(modsData[itemSugar["Options"]]["Fixed"]);
        setCurrSugar(itemSugar["Default"]);
        // set toppings data
        let toppingsData = itemData["Toppings"];
        // setRmToppings(toppingsData["Removable"]);
        setMaxToppings(itemData["MaxToppings"] - toppingsData["Default"].length);
        // setMaxToppings(itemData["MaxToppings"]);
        // setToppingsPre(toppingsData["Default"]);
        // setToppingsPreNum(toppingsData["Default"].length);
        setToppings(modsData[toppingsData["Options"]]["Options"]);
        // set milk option
        setMilkOpt(itemData["Milk"]["MilkOpt"]);
    }, [getDrink]);

    // only store milk values if milk option is available
    useEffect(() => {
        if (milkOpt) {
            let itemMilk = drinksData[getDrink]["Milk"];
            setMilk(modsData[itemMilk["Options"]]["Options"]);
            setMilkPrice(modsData[itemMilk["Options"]]["Price"]);
            // store for local comparison
            setMilkDefault(itemMilk["Default"]);
        };
    }, [milkOpt]);

    // set currentToppings as default if any
    useEffect(() => {
        if (!isEdit) {
            // if (toppingsPre.length === 0) {
                setCurrToppings([]);
                setCurrOrder(currOrder => ({...currOrder, "toppings": [] }));
            // } else {
            //     setCurrToppings(toppingsPre);
            //     setCurrOrder(currOrder => ({...currOrder, "toppings": toppingsPre }));
            // };
        };
    }, [isEdit]);
    // }, [isEdit, toppingsPre]);

    // set price dynamically according to drink, size and additional toppings
    useEffect (() => {
        // get price from data and calculate based on number of toppings
        let price = +(Number(drinksData[getDrink]["Original Price"])).toFixed(2);
        // +size price
        if (currSize) {
            price = +(price + Number(modsData[drinksData[getDrink]["Size"]["Options"]]["Price"][currSize])).toFixed(2);
        }
        // +toppings price
        // if (currToppings.length > toppingsPreNum) {
            let addToppings = [...currToppings];
            // only add cost for toppings beyond num of default toppings
            // addToppings.splice(0, toppingsPreNum);
            addToppings.map((i) => {
                price = +(Number(price) + (modsData[drinksData[getDrink]["Toppings"]["Options"]]["Price"][i])).toFixed(2);
            });
        // };
        // +milk price
        if (currMilk) {
            price = +(price + Number(modsData[drinksData[getDrink]["Milk"]["Options"]]["Price"][currMilk])).toFixed(2);
        };
        // display price
        if (price > 0) {
            setCurrPrice(price);
        } else {
            setCurrPrice(0);
        };

        // check for discounts
        discountAPI(price);

        // update price in currOrder
        // setCurrOrder(currOrder => ({...currOrder, "price": price}));
    }, [getDrink, currSize, currToppings, currMilk]);

    ///////////////////////
    // SET DISPLAY TEXT //
    //////////////////////
    // set display names
    useEffect(() => {
        if (currSize) {
            setDisplaySize(modKeysData[currSize][nameLang]);
        };
    }, [currSize]);

    useEffect(() => {
        if(currIce) {
            setDisplayIce(modKeysData[currIce][nameLang]);
        };
    }, [currIce]);

    useEffect(() => {
        if (currSugar) {
            setDisplaySugar(modKeysData[currSugar][nameLang])
        }
    }, [currSugar]);

    // define subtitles
    useEffect(() => {
        let text = langData["Toppings Subtitle"][lang];
        text = text.replace("{maxToppings}", maxToppings);
        text = text.replace("{basePrice}", baseAddPrice);
        if (maxToppings === 1) {
            text = text.replace("toppings", "topping");
        };
        setToppingsSub(text);
    }, [maxToppings]);

    useEffect(() => {
        if (milkDefault) {
            let milkCost = false;
            let milkPriceDict = modsData[drinksData[getDrink]["Milk"]["Options"]]["Price"];
            Object.keys(milkPriceDict).map((i) => {
                if (milkPriceDict[i] > 0) {
                    milkCost = true;
                };
            });

            let defaultMilkName = modKeysData[milkDefault][nameLang];
            if (milkCost) {
                let text = langData["Milk Subitile"][lang];
                text = text.replace("{basePrice}", baseAddPrice);
                text = text.replace("{defaultMilk}", defaultMilkName);
                setMilkSub(text);
            } else {
                setMilkSub("");
            };
        };
    }, [milkDefault]);

    /////////////////////////
    // USE EFFECT STYLING //
    ////////////////////////
    // add styling to toppings
    useEffect(() => {
        try {
            const toppingsContainer = document.getElementById("toppingsSelect");
            removeAllClass(toppingsContainer, "selectedImg");
            removeAllClass(toppingsContainer, "selectedText");
            
            currToppings.map((i) => {
                // add styling to selected size
                let parentElem = document.getElementById(i);
                let imageElem = parentElem.querySelector("img");
                let pElem = parentElem.querySelector("p");
                imageElem.classList.add("selectedImg");
                pElem.classList.add("selectedText");
            });
        } catch {
            console.log("NO TOPPINGS CONTAINER");
        }
    }, [currToppings]);

    // set modifiers when editing
    useEffect (() => {
        // rest of the modifiers
        if (isEdit) {
            // size
            setCurrSize(editOrder["size"]);
            setCurrOrder(currOrder => ({...currOrder, "size": editOrder["size"]}));
            // ice
            setCurrIce(editOrder["ice"]);
            setCurrOrder(currOrder => ({...currOrder, "ice": editOrder["ice"]}));
            // sugar
            setCurrSugar(editOrder["sugar"]);
            setCurrOrder(currOrder => ({...currOrder, "sugar": editOrder["sugar"]}));
            // toppings
            if (editOrder["toppings"].length !== 0) {
                setCurrToppings(editOrder["toppings"]);
                setCurrOrder(currOrder => ({...currOrder, "toppings": editOrder["toppings"] }));
            } else {
                setCurrToppings([]);
                setCurrOrder(currOrder => ({...currOrder, "toppings": [] }));
            };
            // milk
            setCurrMilk(editOrder["milk"]);
            setCurrOrder(currOrder => ({...currOrder, "milk": editOrder["milk"]}));
        } else {
            // reset
            // setCurrToppings([]);
            let itemData = drinksData[getDrink];
            setCurrSize(itemData["Size"]["Default"]);
            setCurrOrder(currOrder => ({...currOrder, "size": itemData["Size"]["Default"]}));
            setCurrIce(itemData["Ice"]["Default"]);
            setCurrOrder(currOrder => ({...currOrder, "ice": itemData["Ice"]["Default"]}));
            setCurrSugar(itemData["Sugar"]["Default"]);
            setCurrOrder(currOrder => ({...currOrder, "sugar": itemData["Sugar"]["Default"]}));
            setCurrMilk(itemData["Milk"]["Default"]);
            setCurrOrder(currOrder => ({...currOrder, "milk": itemData["Milk"]["Default"]}));
        };
    }, [editOrder]);


    ////////////////
    // FUNCTIONS //
    ///////////////
    // iterate through all the children in a container and remove the identified class
    function removeAllClass(containerName, styleName) {
        // Remove the class if it exists
        if (containerName.classList.contains(styleName)) {
            containerName.classList.remove(styleName);
        };
    
        // Recursively traverse through child elements
        if (containerName.children.length > 0) {
            for (let i = 0; i < containerName.children.length; i++) {
                removeAllClass(containerName.children[i], styleName);
            }
        };
    };
    

    //////////////////////
    // CREATE STICKERS //
    /////////////////////
    let soldoutSticker = <div className="soldoutStickerDrinks">
                            <p className="labelsSmall textWhite">{langData["Sold Out"][lang]}</p>
                        </div>

    let saleSticker = <div className="posRelative">
                            <img className="img8 toLeft spacingLeft" src={saleImg} />
                        </div>
    
    ////////////
    // HEADER //
    ////////////
    function goToCart() {
        if (isEdit) {
            sendOrder(editOrder); // send previous order
        };
        sendState("checkout");
        // reset edit
        sendIsEdit(false);
    };

    // create cart
    let cartStyle = cartDisplay ? "icons selectedIconText" : "icons";
    let cart = <div className="orderIcons" onClick={() => goToCart()}>
                    { cartDisplay && <p className="labels cartLabelTop">{cartNum}</p> }
                    <FontAwesomeIcon className={cartStyle} icon={faCartShopping} size="3x" />
                </div>

    // initialize header for order flow
    let orderHeader = <div className="flexParent orderHeader">
                            <div className="orderText">
                                <p className="title selectedTitle">{displayName}</p>
                            </div>
                            {/* { cart } */}
                        </div>


    ///////////////////
    // DRINK SUMMARY //
    ///////////////////
    let orderSummary = 
        <div className="div60 spacing">
            <div className="flexParent centerItem">
                <div className="flex1">
                    <img className="drinkImage" src={require('../../image/drinks/' + drinksData[getDrink]["Image"])} alt=""/>
                </div>
                <div className="flex1 floatRight textRight">
                    <button className="buttonBlank">
                        <FontAwesomeIcon className="subtitle textGrey" icon={faCircleInfo} onClick={() => setGetInfo(true)}/>
                    </button>
                    <p className="subtitle">{displaySize}</p>
                    <p className="subtitle">{currCalories}</p>
                    { onSale && <p className="buttonSubtitle textGrey textStrike">{displayOldPrice}</p> }
                    <p className="subtitle selectedTitle spacingBig">{displayPrice}</p>
                </div>
            </div>
        </div>
    

    // create drink info popup
    let infoPopup = <div className="popupVeil" onClick={() => setGetInfo(false)}>
                        <div className="infoPopUp">
                            <div className="spacingTop2">
                                <p className="labels">{drinksData[getDrink][describeLang]}</p>
                            </div>
                        </div>
                    </div>


    /////////////////////
    // SIZE SELECTION //
    ////////////////////
    function selectSize(id) {
        setCurrSize(id);
        setCurrOrder(currOrder => ({...currOrder, "size": id}));
    };

    let defaultSizeImage = "Medium.png";
    useEffect(() => {
        if (currSize) {
            defaultSizeImage = modKeysData[currSize]["Name En"] + ".png"
        };
    }, [currSize])

    // initialize fixed size div
    let sizeFixed = <div className="div40">
                        <img className="imgFrameSmall spacing selectedImg" src={require("../../image/order/" + defaultSizeImage)} />
                        <p className="labels selectedText">{displaySize}</p>
                    </div>

    let sizeTitle = <div className="flex1 orderTitles">
                        <p className="title">{langData["Size"][lang]}</p>
                    </div>

    // create dynamic size
    let sizeChoices = size.map((k, i) => {
        let name = modKeysData[k][nameLang];
        let image = modKeysData[k]["Name En"] + ".png"
        let imageStyle = k === currSize ? "imgFrameMed spacing selectedImg" : "imgFrameMed spacing";
        let textStyle = k === currSize ? "subtitle selectedText" : "subtitle";

        return (
            <>
                <div id={k} className="flex2" onClick={() => selectSize(k)}>
                    <img className={imageStyle} src={require("../../image/order/" + image)} />
                    <p className={textStyle}>{name}</p>
                </div>
                { i === 0 && sizeTitle }
            </>
        )
    });

    // compile size 
    let orderSize = <div id="sizeSelection" className="div60 flexParent spacing">
                        { fixedSize ? sizeFixed : sizeChoices  }
                    </div>



    ///////////////////
    // ICE SELECTION //
    ///////////////////
    function selectIce(id) {
        setCurrIce(id);
        setCurrOrder(currOrder => ({...currOrder, 'ice': id}));
    };

    // custom event handler for ice slider
    function iceDragHandler(event) {
        // get coordinates
        let x = event.clientX;
        let y = event.clientY;
        // for touch event
        if (x === undefined && y === undefined) {
            let touchEvent = event.touches[0];
            x = touchEvent.clientX;
            y = touchEvent.clientY;
        };
        // find parent element
        let droppedElement = document.elementFromPoint(x, y).parentNode;
        let selIce = droppedElement.id;

        if (ice.includes(selIce)) {
            selectIce(selIce);
        };
    };

    // create ice header
    let iceHeader = <div className="div60 spacing">
                        <p className="title">{langData["Ice"][lang]}</p>
                    </div>

    // create fixed ice div
    let iceFixed = <div className="div30">
                        <img className="imgFrameSmall selectedImg" src={drinkIcedImg} />
                        <p className="labels selectedText textPadding">{displayIce}</p>
                    </div>

    // calculate the position of control thumb according to default or prev selection
    let iceListLen = ice.length;
    let iceSlideNum = (iceListLen - 4) * -2;
    if (iceListLen <= 3) {
        iceSlideNum = 26 - (iceListLen * 7);
    };
    let iceControlPosition = ((100 / iceListLen) * ice.indexOf(currIce) + iceSlideNum) + "%"

    // create dynamic slider
    let iceChoices = ice.map((k) => {
        let name = modKeysData[k][nameLang];
        // update dot spacing
        let dotPos = String(19 - (2 * iceListLen)) + "%";
        if (iceListLen <= 3) {
            dotPos = (37 - (iceListLen * 7)) + "%";
        };
        // add highlight to default ice
        let textStyle = k === currIce ? "labels sliderSpacing selectedText" : "labels sliderSpacing";

        return (
            <div id={k} className="flex1" onClick={() => selectIce(k)}>
                <div className="dot" style={{marginLeft: dotPos}}></div>
                <hr className="line" />
                <p className={textStyle}>{name}</p>
            </div>
        );
    });    

    // create slider thumb
    const hotDrinks = ["MB_00", "MB_01"];
    let iceThumb = <div id="iceSelect" className="thumb" onTouchMove={iceDragHandler} onDrag={iceDragHandler}>
                        <img className="imgFrameMed selectedImg" src={ hotDrinks.includes(currIce) ? drinkHotImg : drinkIcedImg } />
                    </div>

    // move the control thumb according to default or prev selection
    const iceControl = document.getElementById("iceSelect");
    if (iceControl) {
        iceControl.style.marginLeft = iceControlPosition;
    };

    // compile ice slider
    let iceSlider = <>
                        { !fixedIce && iceHeader }
                        <div className= "sliderContainer flexParent spacing">
                            { fixedIce ?  iceFixed : iceChoices }
                            { fixedIce ? <></> : iceThumb }
                        </div>
                    </>
    


    /////////////////////
    // SUGAR SELECTION //
    /////////////////////
    function selectSugar(id) {
        setCurrSugar(id);
        setCurrOrder(currOrder => ({...currOrder, "sugar": id}));
    };

    // custom event handler for sugar slider
    function sugarDragHandler(event) {
        // get coordinates
        let x = event.clientX;
        let y = event.clientY;
        // for touch event
        if (x === undefined && y === undefined) {
            let touchEvent = event.touches[0];
            x = touchEvent.clientX;
            y = touchEvent.clientY;
        };
        // find parent element
        let droppedElement = document.elementFromPoint(x, y).parentNode;
        let selSugar = droppedElement.id;

        if (sugar.includes(selSugar)) {
            selectSugar(selSugar);
        };
    };

    // create sugar header
    let sugarHeader = <div className="div60 spacing">
                            <p className="title">{langData["Sugar"][lang]}</p>
                        </div>

    // create fixed sugar div
    let sugarFixed = <div className="div30">
                        <img className="imgFrameSmall selectedImg" src={sugarImg} />
                        <p className="labels selectedText textPadding">{displaySugar}</p>
                    </div>


    // calculate the position of control thumb according to default or prev selection
    let sugarListLen = sugar.length;
    let sugarSlideNum = (sugarListLen - 4) * -2;
    if (sugarListLen <= 3) {
        sugarSlideNum = 26 - (sugarListLen * 7);
    };
    let sugarControlPosition = ((100 / sugarListLen) * sugar.indexOf(currSugar) + sugarSlideNum) + "%"

    // create dynamic slider
    let sugarChoices = sugar.map((k) => {
        let name = modKeysData[k][nameLang];
        // format labels
        if (name.includes("%")) {
            name = name.replace(" Sugar", "");
        };
        // update dot spacing
        let dotPos = (19 - (2 * sugarListLen)) + "%";
        if (sugarListLen <= 3) {
            dotPos = (37 - (sugarListLen * 7)) + "%";
        };
        // add styling
        let textStyle = k === currSugar ? "labels sliderSpacing selectedText" : "labels sliderSpacing";

        return (
            <div id={k} className="flex1" onClick={() => selectSugar(k)}>
                <div className="dot" style={{marginLeft: dotPos}}></div>
                <hr className="line"></hr>
                <p className={textStyle}>{name}</p>
            </div>
        )
    });

    // create slider thumb
    let sugarThumb = <div id="sugarControl" className="thumb" onTouchMove={sugarDragHandler} onDrag={sugarDragHandler}>
                        <img className="imgFrameMed selectedImg" src={sugarImg} />
                    </div>
                    
    // move the control thumb according to default or prev selection
    const sugarControl = document.getElementById("sugarControl");
    if (sugarControl) {
        sugarControl.style.marginLeft = sugarControlPosition;
    };

    // compile sugar slider
    let sugarSlider = <>
                        { !fixedSugar && sugarHeader }
                        <div className= "sliderContainer flexParent spacing">
                            { fixedSugar ? sugarFixed : sugarChoices }
                            { fixedSugar ? <></> : sugarThumb }
                        </div>
                    </>


    ///////////////////////
    // TOPPING SELECTION //
    //////////////////////
    // function to select sides
    function addToppings(id) {
        setWarning(false); // reset warning
        let sideNum = currToppings.length;
        if (sideNum < maxToppings) {
            // does not allow duplicate toppings
            if (currToppings.includes(id)) {
                setCurrToppings(list => list.filter(item => item !== id));
                setCurrOrder(currOrder => ({...currOrder, "toppings": currToppings.filter(item => item !== id) }));
            } else {
                setCurrToppings(list => [...list, id]);
                setCurrOrder(dict => ({...dict, 'toppings': [...currToppings, id]}));
            };

        } else if (sideNum === maxToppings) {
            // allow deselect 
            if (currToppings.includes(id)) {
                setCurrToppings(list => list.filter(item => item !== id));
                setCurrOrder(currOrder => ({...currOrder, "toppings": currToppings.filter(item => item !== id) }));
            } else {
                setWarning(true);
            };
        } else {
            setWarning(true);
        };
    };

    // create toppings grid
    let toppingChoices = toppings.map((k) => {
        let name = modKeysData[k][nameLang];
        let imageName = modKeysData[k]["Name En"] + ".png";
        let price = Number(modsData[drinksData[getDrink]["Toppings"]["Options"]]["Price"][k]);

        // only allow click if in stock
        if (stockData[k]) {
            return (
                <div id={k} className="toppingsCell" onClick={() => addToppings(k)}>
                    <img className="imgFrameLarge" src={require("../../image/order/" + imageName)} />
                    <p className="labels textPadding">{name}</p>
                    { price > baseAddPrice && <p className="labelsSmall">+${price}</p> }
                </div>
            )
        } else {
            return (
                <div className="posRelative">
                    { soldoutSticker }
                    <div id={k} className="toppingsCell imageSoldOut">
                        <img className="imgFrameLarge" src={require("../../image/order/" + imageName)} />
                        <p className="labels textPadding">{name}</p>
                        { price > baseAddPrice && <p className="labelsSmall">+${price}</p> }
                    </div>
                </div>
            )
        };

    });

    // compile toppings grid
    let toppingsGrid = <div className="spacingBot8">
                        <div>
                            <p className="title spacingMini">{langData["Toppings"][lang]}</p>
                            <p className="labels toppingsSubtitle spacingMini">{toppingsSub}</p>
                            { warning && <p className="text selectedText spacingBot2">{langData["Toppings Warning"][lang]}</p> }
                        </div>
                        <div id="toppingsSelect" className="toppingsGrid spacing">
                            {toppingChoices}
                        </div> 
                    </div>
    
    

    //////////////////////
    // MILK ALTERNATIVE //
    /////////////////////  
    function selectOatMilk(id) {
        setCurrMilk(id);
        setCurrOrder(currOrder => ({...currOrder, "milk": id}));
    };

    let milkOptions = milk.map((k) => {
        let name = modKeysData[k][nameLang];
        let price = "";
        let milkOpt = drinksData[getDrink]["Milk"]["Options"];
        if (modsData[milkOpt]["Price"][k] > 0) {
            price = "+$" + modsData[milkOpt]["Price"][k];
        };
        let buttonStyle = k === currMilk ? "buttonBlank buttonChoice buttonSelectedRed" : "buttonBlank buttonChoice";
        let textStyle = k === currMilk ? "labels textRed" : "labels";

        if (stockData[k]) {
            return (
                <div id={k} className="flexAuto centerItem" onClick={() => selectOatMilk(k)}>
                    <button className={buttonStyle}>
                        <p className={textStyle}>{name}</p>
                        <p className="labelsSmall">{price}</p>
                    </button>
                </div>
            )
        } else {
            return (
                <div id={k} className="flexAuto centerItem imageSoldOut">
                    <button className={buttonStyle}>
                        <p className={textStyle}>{name}</p>
                        <p className="labelsSmall">{price}</p>
                    </button>
                </div>
            )
        }

    })

    let milkOptSection = <>
                            <div className="spacingMini">
                                <p className="subtitle">{langData["Milk Options"][lang]}</p>
                                {/* <p className="labels toppingsSubtitle spacingMini">{milkSub}</p> */}
                            </div>
                            <div className="flexParent div90 centerItem">
                                { milkOptions }
                            </div>
                        </>


    ////////////////////
    // SUBMIT BUTTON //
    ///////////////////
    // function to submit order
    function submitOrder() {
        if (isEdit) {
            sendState("checkout");
            sendOrder(currOrder);
        } else {
            sendState("confirmOrder");
            sendOrder(currOrder);
        };
        // reset edit
        sendIsEdit(false);
    };

    function cancelOrder() {
        if (isEdit) {
            sendState("checkout");
            sendOrder(editOrder); // send previous order
        } else {
            sendState("");
        };
        // reset edit
        sendIsEdit(false);
    };

    let checkoutFooter = <div className="cartFooter flexParent centerItem">
                            <div className="toLeft spacingLeft">
                                <p className="subtitle textWhite">{displayName}</p>
                                <p className="subtitle textWhite">{displayPrice}</p>
                            </div>
                            <div className="flexParent toRight spacingRight">
                                <button className="buttonBlank buttonAdd" onClick={() => submitOrder()}>
                                    <p className="buttonSubtitle textWhite">
                                        <span><FontAwesomeIcon icon={faCirclePlus}/></span>
                                        <span>&ensp;{langData["Add"][lang]}</span>
                                    </p>
                                </button>
                            </div>
                        </div>
    
    
    console.log(currOrder);

    return (
        <motion.div
            initial={{ opacity: 0.2, }} 
            animate={{ opacity: 1, }} 
            transition={{ duration: 0.5 }}
        >
            <div className="drinkModContainer">
                <BackTab lang={lang} sendBack={cancelOrder} />
                <CartTab lang={lang} sendToCart={goToCart} num={cartNum} />
                { getInfo && infoPopup }
                { orderHeader }
                { onSale && saleSticker }
                { orderSummary }
                { orderSize }
                { iceSlider }
                { sugarSlider }
                { maxToppings > 0 && toppingsGrid } 
                { milkOpt && milkOptSection }
            </div>
            { checkoutFooter }
        </motion.div>
    )
};
