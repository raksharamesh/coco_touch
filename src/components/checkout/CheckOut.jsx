import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import your icons
import { faCircleLeft, faCartShopping, faPenToSquare, faTrashCan, faCircleXmark, faCirclePlus, faCircleMinus, faTrash } from '@fortawesome/free-solid-svg-icons'
// import functions
import { FormatToppings, FormatSummary } from "../../functions/FormatOrder"
import { prettyRounding } from "../../functions/CheckoutCalc";
// import images
import paymentImg from '../../image/payment.png'
import paymentGif from '../../image/payment.gif'
import logo from '../../image/logo.png'
// import data 
import itemData from '../../data/itemsAll.json'
import paymentDevice from '../../data/payment_config.json'
import modsKeyData from '../../data/modKeys.json'
import langData from "../../data/vocab.json"
// import components
import BackTab from "../tools/BackTab";
import CancelPopup from "./CancelPopup";
import TipPopup from "./TipPopup";
import MobilePopup from "./MobilePopup";

// payment variables
const MODE = paymentDevice["MODE"];
const kiosk = paymentDevice['KIOSK_NAME']; 
const store = paymentDevice["STORE_NAME"]; 
const targetURL = paymentDevice[MODE]["APP_HOME"];
const DEVICE = paymentDevice["DEVICE_TYPE"];

export default function CheckOut({ sendData, language, order, cartDisplay, cartNum, orderID, sendState, sendReset, sendPrint }) {
    const [lang, setLang] = useState(language);
    const [nameLang, setNameLang] = useState("Name " + lang);
    const [checkoutData, setCheckoutData] = useState(undefined);
    const [orderData, setOrderData] = useState([]);
    const [paymentStatus, setPaymentStatus] = useState("");
    const [tipAmount, setTipAmount] = useState(0);
    const [orderNum, setOrderNum] = useState("");
    const [subtotal, setSubtotal] = useState(0);
    const [tax, setTax] = useState(0);
    const [total, setTotal] = useState(0);
    const [discountAmt, setDiscountAmt] = useState(0);
    const [displaySubtotal, setDisplaySubtotal] = useState(0);
    const [displayTax, setDisplayTax] = useState(0);
    const [displayTotal, setDisplayTotal] = useState(0);
    const [displayDiscount, setDisplayDiscount] = useState(0);
    const [loadingText, setLoadingText] = useState("Fetching Discounts");
    const [paymentChoice, setPaymentChoice] = useState("");
    // flag
    const [checkoutConfirm, setCheckoutConfirm] = useState(false);
    const [isCancel, setIsCancel] = useState(false);
    const [showTip, setShowTip] = useState(false);
    const [hasTipped, setHasTipped] = useState(false);
    const [showDiscount, setShowDiscount] = useState(false);
    const [getCheckout, setGetCheckout] = useState(false);
    const [askMobile, setAskMobile] = useState(false);

    // define payment data (locally stored)
    let paymentData = {"summary": FormatSummary(orderData), 
                        "payment": {
                            "subtotal": displaySubtotal,
                            "tax": displayTax,
                            "total": displayTotal,
                            "tip": tipAmount,
                            "discount_applied": showDiscount,
                            "discount_amount": displayDiscount
                        }, 
                        "description": {
                            "order_id": orderNum,
                            "order_source": "KIOSK",
                            "order_type": "",
                            "cash": false
                        }
                    };
    

    useEffect(() => {
        setLang(language);
        setNameLang("Name " + language);
    }, [language]);

    // call checkout API when component mounts
    useEffect(() => {
        if (order.length > 0) {
            setLoadingText("Fetching Discounts");
            setGetCheckout(true);
            POSTcheckout(FormatToppings(order));
        };
    }, []);

    // update order details and payment data after checkout API call
    useEffect(() => {
        if (checkoutData) {
            setOrderData(Object.values(checkoutData["final_order"]["summary"]));
            setSubtotal(checkoutData["final_order"]["payment"]["subtotal"]);
            setTax(checkoutData["final_order"]["payment"]["tax"]);
            setTotal(checkoutData["final_order"]["payment"]["total"]);
            setDiscountAmt(checkoutData["final_order"]["payment"]["discount_amount"]);
            setShowDiscount(checkoutData["final_order"]["payment"]["discount_applied"]);
        };
    }, [checkoutData]);

    // format text
    useEffect(() => {
        setDisplaySubtotal(prettyRounding(subtotal));
        setDisplayTax(prettyRounding(tax));
        setDisplayTotal(prettyRounding(total));
        setDisplayDiscount(prettyRounding(discountAmt));
    }, [subtotal, tax, total, discountAmt]);

    useEffect(() => {
        if (showDiscount) {
            // display subtotal should be original subtotal
            let discounted = prettyRounding(subtotal + discountAmt);
            setDisplaySubtotal(discounted);
        };
    }, [showDiscount])

    // POST payment once tip is selected
    useEffect(() => {
        if (hasTipped) {
            showMobile("CARD");
        };
    }, [hasTipped]);


    ////////////
    // HEADER //
    ////////////
    let cartStyle = cartDisplay ? "icons selectedIconText" : "icons";
    let cart = <div className="orderIcons">
                    { cartDisplay && <p className="labels cartLabelTop">{cartNum}</p> }
                    <FontAwesomeIcon className={cartStyle} icon={faCartShopping} size="3x"/>
                </div>

    // initialize header for order flow
    let orderHeader = <div className="spacingBot8">
                        <div className="flexParent orderHeader">
                            <div className="orderText">
                                <p className="title selectedTitle">{langData["Cart"][lang]}</p>
                            </div>
                            {/* { cart } */}
                        </div>
                    </div>

    ////////////////
    // FUNCTIONS //
    ///////////////
    // navigate
    function goBack() {
        sendState("pickDrink");
    };

    // function to remove drink from order 
    function removeDrink(data) {
        setOrderData(orderData.filter((i) => i !== data));
        sendData(data, "edit", false);
        // set loading
        if (orderData.filter((i) => i !== data).length > 0) {
            setLoadingText("Updating the Cart");
            setGetCheckout(true);
            POSTcheckout(FormatToppings( orderData.filter((i) => i !== data) ));
        };
    };

    // function to edit drink
    function editDrink(data) {
        sendData(data, "edit", true);
        sendState("drinkFlow");
    };

    // function edit quantity
    function add1(data) {
        let updatedItem = data;
        updatedItem.quantity += 1;
        // replace previous item with updated item
        setOrderData(orderData.map((e) => e === data ? updatedItem : e));
        sendData(updatedItem, "editQuantity");
        // set loading
        setLoadingText("Updating the Cart");
        setGetCheckout(true);
        POSTcheckout(FormatToppings( orderData.map((e) => e === data ? updatedItem : e) ));
    };

    function remove1(data) {
        if (data.quantity === 1) {
            removeDrink(data)
        } else {
            let updatedItem = data;
            updatedItem.quantity -= 1;
            // replace previous item with updated item
            setOrderData(orderData.map((e) => e === data ? updatedItem : e));
            sendData(updatedItem, "editQuantity");
            // set loading
            setLoadingText("Updating the Cart");
            setGetCheckout(true);
            POSTcheckout(FormatToppings( orderData.map((e) => e === data ? updatedItem : e) ));
        };
    };

    // function to reset if cancel is confirmed
    function confirmCancel(bool) {
        if (bool) {
            sendReset(true);
        } else {
            setIsCancel(false);
        };
    };

    // function to print receipt
    function printReceipt(data) {
        let printData = JSON.stringify(data);
        sendPrint(printData);
    };

    // function to try again
    function tryAgain() {
        POSTcheckout(FormatToppings(order));
        setCheckoutConfirm(false);
        setPaymentStatus("");
    };

    // functions to show and hide MobilePopup
    function showMobile(payment) {
        // only go to mobile option if cart
        if (DEVICE === "CART") {
            setAskMobile(true);
            setPaymentChoice(payment);
        } else {
            if (payment === "CASH") {
                POSTcashData();
            } else if (payment === "CARD") {
                POSTcardData(paymentData);
            };
        };
    };

    function hideMobile() {
        setAskMobile(false);
        if (paymentChoice === "CASH") {
            POSTcashData();
        } else if (paymentChoice === "CARD") {
            POSTcardData(paymentData);
        };
    };



    ///////////////////
    // CHECKOUT API //
    //////////////////
    async function POSTcheckout(order, retries = 3) {
        // set post URL
        let postURL = targetURL + '/checkout';
        let checkoutOrder = {"summary": FormatSummary(order)};

        // initialize data to send to backend
        let data = JSON.stringify({ 
            device_serial_id: paymentDevice[MODE][store][kiosk]["DEVICE_SERIAL_ID"],
            merchant_id: paymentDevice[MODE][store]["MERCHANT_ID"],
            location_id: paymentDevice[MODE][store]["LOCATION_ID"],
            final_order: checkoutOrder,
            order_id: orderID
        });

        console.log("Checkout data ", data);

        // try 3 times before giving up
        for (let attempt = 1; attempt <= retries; attempt++) {
            try {
                const res = await fetch(postURL, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: data
                });
                console.log("POST Response ", res.statusText, res.status);
    
                if (res.status === 200) {
                    const result = await res.json();
                    console.log("RESULT checkout ", result, result.status);
    
                    if (result.status === "CHECKOUT SUCCESS") {
                        setCheckoutData(result["cart"]);
                        setGetCheckout(false);
                    };
                    return result.status;
                } else {
                    console.log("Error checkout ", res.status);
                    if (attempt === retries) {
                        // TEMP: fail 3 times ask them to retry
                        setCheckoutConfirm(true);
                        setPaymentStatus("PAYMENT FAIL");
    
                        console.error("All attempts failed. Error at checkout APi");
                        return 'ERROR';
                    };
                };
    
            } catch (err) {
                console.log(`Attempt ${attempt} failed:`, err);
    
                if (attempt === retries) {
                    // TEMP: fail 3 times ask them to retry
                    setCheckoutConfirm(true);
                    setPaymentStatus("PAYMENT FAIL");

                    console.error("All attempts failed. Error checkout ", err);
                    return 'ERROR';
                };
            };
        };

    };


    ///////////////////
    // PAYMENT APIS //
    //////////////////
    // function for payment GET request
    function GETpayment(taskID) {
        let getURL = targetURL + "/task_status/" + taskID;
        return new Promise((resolve, reject) => {
            const pollInterval = setInterval(() => {
                fetch(getURL).then((res) => {
                    res.json().then((serverData) => {
                        console.log("GET Response ", serverData.status, serverData.message);
    
                        if (serverData.status === "SUCCESS" || serverData.status === "FAILURE" || serverData.status === "CANCELED") {
                            console.log("Payment result ", serverData.result);
                            clearInterval(pollInterval);
                            resolve(serverData.result);
                        } else if (serverData.status === "PENDING") {
                            // set pending status for loading animation
                            setPaymentStatus(serverData.status);
                        } else {
                            console.log("Payment is still being processed. Checking again in 3 seconds...");
                        };
                    }).catch((err) => {
                        clearInterval(pollInterval);
                        reject(err);
                    });
    
                });

            }, 3000); // poll every 3 seconds
            
        });
    };

    ///////////////////
    // CARD PAYMENT //
    //////////////////
    // function to post data
    function POSTcardData(order) {
        // show payment animation
        setCheckoutConfirm(true);
        // reset tipped flag
        setHasTipped(false);

        // set post URL
        let postURL = targetURL + '/init_payment';

        // initialize data to send to backend
        let data = JSON.stringify({ 
            device_serial_id: paymentDevice[MODE][store][kiosk]["DEVICE_SERIAL_ID"],
            merchant_id: paymentDevice[MODE][store]["MERCHANT_ID"],
            location_id: paymentDevice[MODE][store]["LOCATION_ID"],
            final_order: order,
            price: order.payment.subtotal,
            tax: order.payment.tax,
            order_id: orderID,
        });
        ///////////////////////////
        // POST data to backend //
        //////////////////////////
        fetch(postURL, {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
            },
            body: data
        }).then((res) => {
            console.log("POST Response ", res.statusText, res.status);

            // if api call goes through successfully
            if (res.status === 202) {
                console.log("Polling for payment status");

                // then GET request poll for payment status 
                res.json().then((apiData) => {
                    // call GET request function
                    GETpayment(apiData.task_id).then((result) => {
                        console.log("RESULT ", result.status);
                        setPaymentStatus(result.status);

                        // update order number
                        if (result.status === "PAYMENT SUCCESS") {
                            paymentData["description"]["order_id"] = result.orderNum; 
                            // set order number
                            let num = result.orderNum.replace(/^\D+/g, '')
                            setOrderNum(num);
                            printReceipt(paymentData);
                        };

                        return result.status;
                    }).catch((err) => {
                        console.log("Error ", err);
                        setPaymentStatus("PAYMENT FAIL");
                        console.log(paymentStatus);
                    }, 10000);
                }).catch((err) => {
                    console.log("Error ", err);
                    setPaymentStatus("PAYMENT FAIL");
                    console.log(paymentStatus);
                }, 10000);

            } else {
                setPaymentStatus("PAYMENT FAIL");
                console.log(paymentStatus);
            };

        }).catch((err) => {
            console.log("Error ", err);
            setPaymentStatus("PAYMENT FAIL");
        });

    };

    /////////////////////
    // POST CASH DATA //
    ////////////////////
    function POSTcashData() {
        setCheckoutConfirm(true);
        setPaymentStatus("CASH PENDING");

        // set tip to 0
        setTipAmount(0);
        paymentData["payment"]["tip"] = 0;
        paymentData["description"]["cash"] = true;

        // set post URL
        let postURL = targetURL + '/cashorder/create_order'; // prod

        // initialize data to send to backend
        let cashData = JSON.stringify({ 
            device_serial_id: paymentDevice[MODE][store][kiosk]['DEVICE_SERIAL_ID'],
            merchant_id: paymentDevice[MODE][store]['MERCHANT_ID'],
            location_id: paymentDevice[MODE][store]['LOCATION_ID'],
            final_order: paymentData,
            price: paymentData.payment.subtotal,
            tax: paymentData.payment.tax,
            order_id: orderID,
        });

        fetch(postURL, {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
            },
            body: cashData
        }).then((res) => {
            console.log("POST Response for Cash ", res.statusText, res.status);

            // if api call goes through successfully
            if (res.status === 200) {
                console.log("Cash API call was successful");
                return res.json();
            } else {
                // default print receipt
                printReceipt(paymentData);
                setPaymentStatus("CASH SUCCESS");
            };

        }).then((response) => {
            console.log("Cash API response ", response);
            setPaymentStatus("CASH SUCCESS");
            // set order number
            setOrderNum(response.orderNum);
            paymentData["description"]["order_id"] = response.orderNum; 
            console.log("FULL ORDER CASH SUCCESS ", paymentData);
            // then print receipt
            printReceipt(paymentData);

            // reset app after 12 seconds
            setTimeout(sendReset, 12000, true);
        }).catch((err) => {
            console.log("Error ", err);
            // default print receipt
            printReceipt(paymentData);
            setPaymentStatus("CASH SUCCESS");

            // reset app after 12 seconds
            setTimeout(sendReset, 12000, true);
        });
    };

    console.log(paymentStatus);


    ////////////////////
    // ORDER SUMMARY //
    ///////////////////
    // iterate through all the drinks in the order and create individual rows
    let drinkSummary = orderData.map((d) => {
        // extract common data
        let id = d.ID;
        let displayName = itemData[id][nameLang];
        let quantity = d.quantity;
        let cat = d.category;
        let price = d.price;
        let originalPrice = prettyRounding(itemData[id]["Original Price"]);
        let discounted = itemData[id]["Discounted"];
        price = prettyRounding(price * quantity); // price including quantity

        if (cat === "Drinks") {
            // extract data
            let size = d.size;
            let ice = d.ice;
            let sugar = d.sugar;
            let toppings = d.toppings;
            let milk = d.milk;
            // create display names
            let displaySize = modsKeyData[size][nameLang];
            let displayIce = modsKeyData[ice][nameLang];
            let displaySugar = modsKeyData[sugar][nameLang];
            let displayToppings = [];
            let displayMilk = "";

            // stringify toppings details
            let toppingsText = "";
            console.log("TOPPINGS ", toppings, toppings.length)
            if (toppings.length === 0) {
                toppingsText = langData["No Additional Toppings"][lang];
            } else {
                toppings.map((i) => {
                    let name = modsKeyData[i][nameLang];
                    displayToppings.push(name);
                });
                toppingsText = displayToppings.join(", ");
            };
            // milk
            if (milk) {
                displayMilk = modsKeyData[milk][nameLang];
            };

            return (
                <div className="div80 flexParent spacing">
                    <div className="flex1 centerItem">
                        <button className="buttonBlank exitContainerLeft" onClick={() => removeDrink(d)}>
                            <FontAwesomeIcon className="buttonText textGrey" icon={faCircleXmark} />
                        </button>
                        <img className="checkoutImg" src={require('../../image/drinks/' + itemData[id]['Image'])} />
                    </div>
                    <div className="flex2">
                        <p className="subtitle selectedTitle textLeft spacingBot3">{displayName}</p>
                        <div className="flexParent">
                            <div className="modList flex3">
                                <p className="labels textLeft bold">{displaySize}</p>
                                <p className="labels textLeft">{displayIce}, {displaySugar}</p>
                                <p className="labels textLeft">{toppingsText}</p>
                                <p className="labels textLeft spacing">{displayMilk}</p>
                            </div>
                            <div className="flex1">
                                <div className="floatRight">
                                    <FontAwesomeIcon className="infoIcons textGrey subtitle spacing" icon={faPenToSquare} onClick={() => editDrink(d)}/>
                                    <p className="labelsSmall">{langData["Edit"][lang]}</p>
                                </div>
                            </div>
                        </div>
                        <div className="gridCol2 floatRight">
                            <p className="subtitle textGrey textStrike">{ discounted && `$${originalPrice}` }</p>
                            <p className="subtitle">${price}</p>
                        </div>
                    </div>
                </div>
            )
        } else {
            let quantityEdit = <div className="quantEditContainer centerItem">
                                    <FontAwesomeIcon className="buttonText textGrey" icon={faCirclePlus} onClick={() => add1(d)} />
                                    <p className="subtitle">{quantity}</p>
                                    <FontAwesomeIcon className="buttonText textGrey" icon={ quantity == 1 ? faCircleXmark : faCircleMinus} onClick={() => remove1(d)} />
                                </div>
            return (
                <div className="div80 flexParent spacing">
                    <div className="flex1 centerItem">
                        { quantityEdit }
                        <img className="checkoutImg" src={require('../../image/drinks/' + itemData[id]['Image'])} />
                    </div>
                    <div className="flex2">
                        <p className="subtitle selectedTitle textLeft spacingBot3">{displayName}</p>
                        <p className="subtitle floatRight">${price}</p>
                    </div>
                </div>
            )
        };
    });


    //////////////////////
    // PAYMENT SUMMARY //
    ////////////////////
    let discountText = <>
                            <p className="paddingLeft labelsList selectedText">{langData["Discount"][lang]}</p>
                            <p className="paddingLeft subtitle textLeft selectedText">-${displayDiscount}</p>
                        </>

    let paymentSummary = <div className="flexParent div70 spacingTop10 spacingBig">
                            <div className="flex2">
                                <img className="paymentImage" src={paymentImg}/>
                            </div>
                            
                            <div className="flex2 paymentGrid centerItem">
                                <p className="paddingLeft labelsList">{langData["Subtotal"][lang]}</p>
                                <p className="paddingLeft subtitle textLeft">${displaySubtotal}</p>
                                { showDiscount && discountText }
                                <p className="paddingLeft labelsList">{langData["Tax"][lang]}</p>
                                <p className="paddingLeft subtitle textLeft">${displayTax}</p>
                                <p className="paddingLeft labelsList bold">{langData["Total"][lang]}</p>
                                <p className="paddingLeft subtitle textLeft selectedText">${displayTotal}</p>
                            </div>
                        </div>

    // confirm payment
    let confirmPayment = <div className="div60">
                            <p className="middletitle textOrange spacingBot5">{langData["Confirm Order"][lang]}</p>
                            <div className="flexParent spacingBot5">
                                <div className="flex1 centerItem" onClick={() => showMobile("CASH")}>
                                    <button className="buttonBlank buttonSubmit bgOrangeTransparent">
                                        <p className="buttonSubtitle">{langData["Cash"][lang]}</p>
                                    </button>
                                </div>
                                <div className="flex1 centerItem">
                                    <button className="buttonBlank buttonSubmit bgOrangeTransparent" onClick={() => setShowTip(true)}>
                                        <p className="buttonSubtitle textWhite">{langData["Card"][lang]}</p>
                                    </button>
                                </div>
                            </div>
                            <div className="centerItem" onClick={() => setIsCancel(true)}>
                                <button className="buttonBlank buttonSubmit spacingBig">
                                    <p className="buttonSubtitle textWhite">{langData["Cancel Order"][lang]}</p>
                                </button>
                            </div>
                        </div>    

    //////////////////////////
    // GENERATE UI ELEMENTS //
    /////////////////////////    
    // initialize animation divs
    let arrow = <div className="spacingBig div30">
                    <div className="arrow down1"></div>
                    <div className="arrow down2"></div>
                    <div className="arrow down3"></div>
                </div>

    let arrowRight = <div className="arrowRight spacingBig div30">
                    <div className="arrow down1"></div>
                    <div className="arrow down2"></div>
                    <div className="arrow down3"></div>
                </div>

    let loading = <div className="loadingRing">
                    Processing
                    <span></span>
                </div>

    let loadingPrint = <div className="loadingRing">
                            Printing
                            <span></span>
                        </div>

    let loadingCheckout = <div className="loadingRing">
                                <p className="doubleLine">{loadingText}</p>
                                <span></span>
                            </div>

    // checkout page
    let summary = <>
                    { drinkSummary }
                    { paymentSummary }
                    { confirmPayment }
                </>

    let emptyCartText = langData["Empty Cart"][lang]
    let emptyCart = <div className="emptyMessage">
                        <p className="subtitle">{emptyCartText}</p>
                    </div>

    let checkoutSummary = <>
                                <BackTab lang={lang} sendBack={goBack} />
                                { orderHeader }
                                { orderData.length > 0 ? summary : emptyCart }
                            </>

let checkoutPage = getCheckout ? loadingCheckout : checkoutSummary;

    // payment page
    let paymentPage;
    // change UI depending on payment status
    if (paymentStatus === "PENDING") {
        paymentPage = <div className="div80 paymentInit">
                        { loading }
                    </div>

    } else if (paymentStatus === "CASH PENDING") {
        paymentPage = <div className="div80 paymentInit">
                        { DEVICE === "CART" ? loading : loadingPrint }
                    </div>

    } else if (paymentStatus === "PAYMENT SUCCESS") {
        paymentPage = <div className="div80 paymentInit">
                        <p className="title selectedTitle spacing">{langData["Card Payment Success"][lang]}</p>
                        <p className="title selectedTitle">{`${langData["Order Number"][lang]} ${orderNum}`}</p>
                        <img className="paymentAnim" src={logo} />
                    </div>

        // reset app after 5 seconds
        setTimeout(sendReset, 5000, true);

    } else if (paymentStatus === "PAYMENT FAIL" || paymentStatus === "PAYMENT CANCELED" || paymentStatus === "PAYMENT TIMEOUT") {
        paymentPage = <div className="div90 paymentInit">
                        <p className="title selectedText spacingBot2">{langData["Card Payment Error"][lang]}</p>
                        <div className="flexParent centerItem">
                            <button className="buttonBlank buttonLarge" onClick={() => tryAgain()}>
                                <p className="title textWhite spacing">{langData["Try Again"][lang]}</p>
                                <FontAwesomeIcon className="title textWhite" icon={faCartShopping} />
                            </button>
                            <button className="buttonBlank buttonLarge bgOrange" onClick={() => sendReset(true)}>
                                <p className="title textWhite spacing">{langData["Cancel Order"][lang]}</p>
                                <FontAwesomeIcon className="title textWhite" icon={faTrashCan} size="5x" />
                            </button>
                        </div>
                    </div>

    } else if (paymentStatus === "CASH SUCCESS") {
        let text = DEVICE === "CART" ? langData["Cash Instructions Cart"][lang] : langData["Cash Instructions"][lang];
        let orderDisplay = DEVICE === "CART" ?
                            <>
                                <p className="subtitle selectedTitle">{langData["Order Number"][lang]}</p>
                                <p className="titleXL textRed">{orderNum}</p>
                            </>
                            : <p className="title selectedTitle">{`${langData["Order Number"][lang]} ${orderNum}`}</p>

        paymentPage = <div className="div80 paymentInit">
                        <p className="title selectedTitle spacingBig">{ text }</p>
                        { orderNum && orderDisplay }
                        { DEVICE !== "CART" && arrow }
                    </div>

        // reset app after 5 seconds
        if (DEVICE === "CART") {
            // increase timeout for cart
            setTimeout(sendReset, 10000, true);
        } else {
            setTimeout(sendReset, 5000, true);
        };
    } else {
        let text = DEVICE === "CART" ? langData["Card Instructions Right"][lang] : langData["Card Instructions Below"][lang];
        paymentPage = <div className="div80 paymentInit">
                        <p className="title selectedTitle">{ text }</p>
                        <img className="paymentAnim" src={paymentGif} />
                        <div className="centerItem">
                            { DEVICE === "CART" ? arrowRight :  arrow }
                        </div>
                    </div>
    };


    return (
        <>
            { showTip && <TipPopup language={lang} subtotal={displaySubtotal} sendTip={setTipAmount} sendShowTip={setShowTip} sendPayment={setHasTipped} /> }
            { askMobile && <MobilePopup language={lang} order={orderID} sendNext={hideMobile} /> }
            { isCancel && <CancelPopup language={lang} sendCancel={confirmCancel} /> }
            { checkoutConfirm ? paymentPage : checkoutPage }
        </>
    )
};