import React, { useState, useEffect } from "react";
// import functions
import { calcTotal, applyPercentage } from "../../functions/CheckoutCalc";
// import data
import langData from "../../data/vocab.json"

// tip amount
const tipData = {
    "10%": 0.1,
    "15%": 0.15,
    "18%": 0.18
};

export default function TipPopup({ language, subtotal, sendTip, sendShowTip, sendPayment }) {
    const [lang, setLang] = useState(language);
    let total = calcTotal(subtotal);

    useEffect(() => {
        setLang(language)
    }, [language]);

    // function to set tip
    function selectTip(t) {
        sendTip(t);
        sendShowTip(false);
        // once tip is selected post data
        sendPayment(true);
    };

    let tipOptions = Object.keys(tipData).map((k) => {
        let tipCalc = Number(tipData[k])
        let tipTotal = applyPercentage(subtotal, tipCalc); //(Math.round( (subtotal * tipCalc) * 100 ) / 100).toFixed(2) // pre-tax
        let displayTipTotal = " ($" + tipTotal + ")"

        return (
            <button className="buttonBlank buttonTip spacing" onClick={() => selectTip(tipTotal)}>
                <p className="buttonSubtitle">
                    <span>{k}</span>
                    <span className="text textWhite">{displayTipTotal}</span>
                </p>
            </button>
        )
    });

    return (
        <div className="popupVeil">
            <div className="popupContainer">
                <p className="middletitle">{langData["Add a Tip"][lang]}</p>
                <p className="text spacingBig">{langData["Total"][lang]} ${total}</p>
                { tipOptions }
                <button className="buttonBlank buttonTip spacing" onClick={() => selectTip(0)}>
                    <p className="buttonSubtitle">{langData["No Tip"][lang]}</p>
                </button>
            </div>
        </div>
    )
};