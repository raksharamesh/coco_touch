import React, { useState, useEffect } from "react";
// import data
import langData from "../../data/vocab.json"

export default function CancelPopup({ language, sendCancel }) {
    const [lang, setLang] = useState(language);
    useEffect(() => {
        setLang(language);
    }, [language]);
    
    return (
        <div className="popupVeil">
            <div className="popupContainer cancelContainer">
                <p className="buttonText selectedText bold spacingBot5">{langData["Confirm Reset"][lang]}</p>
                <div className="flexAuto centerItem">
                    <div className="centerItem">
                        <button className="buttonBlank buttonSubmitPopup" onClick={() => sendCancel(false)}>
                            <p className="buttonSubtitle">{langData["No"][lang]}</p>
                        </button>
                    </div>
                    <div className="centerItem">
                        <button className="buttonBlank buttonSubmitPopup" onClick={() => sendCancel(true)}>
                            <p className="buttonSubtitle">{langData["Yes"][lang]}</p>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
};