import React, { useState, useEffect } from "react";
import Input from 'react-phone-number-input/input'
// import data
import langData from "../../data/vocab.json"
// import function
import { POSTPhoneNumber } from "../../functions/APIfunctions";

export default function MobilePopup({ language, order, sendNext }) {
    const [lang, setLang] = useState(language);
    const [phoneNumber, setPhoneNumber] = useState("");
    const [warningText, setWarningText] = useState(langData["Valid Number"][lang]);
    // flag
    const [inputWarning, setInputWarning] = useState(false);

    useEffect(() => {
        setLang(language)
    }, [language]);

    // only allow user to submit number if number is valid
    useEffect (() => {
        const numberConfirm = document.getElementById("numberConfirm");

        if (phoneNumber !== undefined) {
            if (phoneNumber.length >= 12) {
                numberConfirm.setAttribute("flag", true);
                numberConfirm.classList.add("bgOrange");
            } else {
                numberConfirm.setAttribute("flag", false);
                numberConfirm.classList.remove("bgOrange");
            };
        };

    }, [phoneNumber]);

    ////////////////
    // FUNCTION //
    ///////////////
    async function phoneNumberAPI() {
        try {
            const newData = await POSTPhoneNumber(order, phoneNumber);
            if (newData.status === "Phone number recorded") {
                sendNext();
            } else if (newData.status === "Try Again") {
                setInputWarning(true);
                setWarningText(langData["Try Again"][lang]);
            } else {
                setInputWarning(true);
                setWarningText(newData.status);
            };
        } catch (error) {
            setWarningText("Please try again.");
            setInputWarning(true);
        };
    };

    function submitNumber() {
        // add error handling
        if (phoneNumber === undefined || phoneNumber.length < 12) {
            setInputWarning(langData["Valid Number"][lang]);
            setInputWarning(true);
        } else {
            phoneNumberAPI();
        };
    };

    return (
        <div className="popupVeil">
            <div className="popupContainer">
                <p className="text buttonText textMedGrey bold">{langData["Ask Receipt"][lang]}</p>
                <p className="text labelsText spacing">{langData["Ask Mobile"][lang]}</p>
                { inputWarning && <p className="text selectedText spacing">{warningText}</p> }
                <div className="spacingBig div100 centerItem formFrame">
                    <Input id="NumberInput" className="formInput text"
                        onClick={() => setInputWarning(false)}
                        defaultCountry="US"
                        value={phoneNumber}
                        onChange={setPhoneNumber}
                        maxLength="16"
                        placeholder="(000) 000-0000"
                    />
                </div>
                <div className="flexParent">
                    <div className="flex1 centerItem">
                        <button className="buttonBlank buttonSmall" onClick={() => sendNext()}>
                            <p className="buttonSubtitle">{langData["Skip"][lang]}</p>
                        </button>
                    </div>
                    <div className="flex1 centerItem">
                        <button id="numberConfirm" className="buttonBlank buttonSmall" onClick={() => submitNumber()}>
                            <p className="buttonSubtitle">{langData["Confirm"][lang]}</p>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
};