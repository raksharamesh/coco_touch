import React, { useState, useEffect } from 'react';
import './App.css';
// import components
import LockScreen from './components/flow/LockScreen';
import ImageSlideShow from './components/tools/ImageSlideShow';
import CategoryNav from './components/category/CategoryNav';
import CategorySelect from './components/category/CategorySelect';
import ItemSelect from './components/category/ItemSelect';
import Header from './components/category/Header';
import DrinkMod from './components/flow/DrinkMod';
import ItemPopup from './components/flow/ItemPopup';
import CheckOut from './components/checkout/CheckOut';
import FilterPopup from './components/tools/FilterPopup';
import CheckoutFooter from './components/tools/CheckoutFooter';
import PythonWebsocket from './components/tools/WebSocket';
import LanguageTab from './components/tools/LanguageTab';
// import data
import catData from "./data/category.json";
import langData from "./data/vocab.json";
import paymentData from "./data/payment_config.json";
// import functions
import GenerateUUID from './functions/GenerateUUID';
import { GETLock } from './functions/APIfunctions';
import { DeepEqual } from "./functions/HelperFunctions";


// initialize empty array to store orders
let orderCart = [];
// get device type
const DEVICE = paymentData["DEVICE_TYPE"];

export default function App({ active }) {
    const [isLocked, setIsLocked] = useState(false);
    const [userID, setUserID] = useState(GenerateUUID());
    const [lang, setLang] = useState("En");
    const [state, setState] = useState("");
    const [filterDrinks, setFilterDrinks] = useState([]);
    const [title, setTitle] = useState(langData["Welcome to CoCo"][lang]);
    const [category, setCategory] = useState("home");
    const [item, setItem] = useState("");
    const [cartNum, setCartNum] = useState("");
    const [editOrder, setEditOrder] = useState({}); // order to edit (from checkOut)
    const [printOrder, setPrintOrder] = useState(undefined); // for websocket
    // flag
    const [isHome, setIsHome] = useState(false);
    const [cartFill, setCartFill] = useState(false);
    const [filterPopup, setFilterPopup] = useState(false);
    const [isEdit, setIsEdit] = useState(false);

    // ping the management backend for lock data every 10 seconds 
    useEffect(() => {
        async function callAPI() {
            const newData = await GETLock();
            if (newData) {
                setIsLocked(newData["LOCK"]);
            } else {
                // data is undefined
                setIsLocked(false);
            };
        };
        callAPI();
        const interval = setInterval(callAPI, 10000); // ping every 10 seconds
    
        // clear interval on component unmount
        return () => clearInterval(interval);
      }, []);

    useEffect(() => {
        if (DEVICE === "CART") {
            setIsHome(false);
            setCategory("all");
        } else {
            setIsHome(true);
            setCategory("home");
        };
    }, []);
    
    useEffect(() => {
        if (isHome) {
            setTitle(langData["Welcome to CoCo"][lang]);
            setCategory("home");
        } else {
            if (DEVICE === "CART") {
                setTitle(langData["Welcome to CoCo"][lang]);
            };
        };
    }, [isHome, lang]);

    // dynamically change cart icon based on cartNum
    useEffect(() => {
        if (cartNum >= 1) {
            setCartFill(true);
        } else {
            setCartFill(false);
        };
    }, [cartNum]);

    // set title as category 
    useEffect(() => {
        if (category !== "home") {
            setIsHome(false);
            if (category !== "all") {
                setTitle(catData[category]["Name " + lang]);
            };
        };
    }, [category, lang]);

    useEffect(() => {
        if (!isEdit) {
            setEditOrder({});
        };
    }, [isEdit]);

    /////////////////////////////
    // handler for inactivity //
    ////////////////////////////
    // only reset the app when user is not active AND state is not voice
    useEffect(() => {
        if (!active) {
            resetApp(true);
        };
    }, [active]);

    ////////////////
    // FUNCTIONS //
    ///////////////
    function resetApp(reset) {
        if (reset) {
            console.log("RESET APP");
            orderCart = [];
            setIsLocked(false);
            setUserID(GenerateUUID());
            setLang("En")
            setState("");
            setFilterDrinks([]);
            setTitle(langData["Welcome to CoCo"][lang]);
            setCategory("home");
            setItem("");
            setCartNum("");
            setEditOrder({}); 
            setPrintOrder(undefined)
            // flag
            setIsHome(false);
            setCartFill(false);
            setFilterPopup(false);
            setIsEdit(false);
        };
    };

    // count up items in cart including quantity
    function tallyItems() {
        let quantity = 0;
        orderCart.map((i) => {
            quantity += i.quantity;
        });
        setCartNum(quantity);
    };

    // function compile items (NOT DRINKS)
    function collapseItems(orderCart, order) {
        let isDuplicate = false; // default false

        let processOrder = orderCart.map(o => {
            if (o.category === "Drinks") {
                return;
            } else {
                if (o.ID === order.ID) {
                    o.quantity = parseFloat(o.quantity) + parseFloat(order.quantity);
                    isDuplicate = true; 
                    return o;
                } else {
                    return;
                };
            };
        });

        if (isDuplicate) {
            // remove old order and then add updated order
            orderCart = orderCart.filter(o => o !== order);
            orderCart.push(processOrder[0]);
        } else {
            orderCart.push(order);
        };
    }

    ///////////////////////////
    // FROM CHILD TO PARENT //
    //////////////////////////
    // from DrinkMod to App
    function addToCart(order) {
        // add order to cart
        if (order !== null) {
            // orderCart.push(order);
            collapseItems(orderCart, order);
        };
        tallyItems();
    };
    
    // from CheckOut to App
    function fromCheckOut(order, type, edit) {
        // remove order for both edit and edit quantity
        orderCart = orderCart.filter((o) => !DeepEqual(o, order));
        // orderCart = orderCart.filter((o) => o !== order);
        if (type === "edit") {
            // set order to edit
            if (edit) {
                setIsEdit(edit);
                setEditOrder(order);
                setItem(order.ID);
            };
            tallyItems();
        } else if (type === "editQuantity") {
            orderCart.push(order);
            tallyItems();
        };
    };
    console.log("IN APP ", orderCart, category, isHome);
    


    ////////////////////////
    // RENDER COMPONENTS //
    ///////////////////////
    let content;
    if (state === "drinkFlow") {
        content = <>
                    <DrinkMod sendOrder={addToCart} language={lang} getDrink={item} cartDisplay={cartFill} cartNum={cartNum} editOrder={editOrder} isEdit={isEdit} sendState={setState} sendIsEdit={setIsEdit} />
                  </>
    } else if (state === "checkout") {
        content = <>
                      <CheckOut sendData={fromCheckOut} language={lang} order={orderCart} category={category} cartDisplay={cartFill} cartNum={cartNum} orderID={userID} sendState={setState} sendReset={resetApp} sendPrint={setPrintOrder} />
                  </>
    } else {
        content = <>
                    <ImageSlideShow />
                    { DEVICE !== "CART" && <CategoryNav language={lang} category={category} sendCat={setCategory} /> }
                    <LanguageTab language={lang} sendLang={setLang} />
                    <div className="catContainer">
                        <Header language={lang} title={title} cartNum={cartNum} isHome={isHome} sendState={setState} sendFilter={setFilterDrinks} sendOpen={setFilterPopup} sendHome={setIsHome} />
                        { DEVICE === "CART" 
                            ? <ItemSelect language={lang} filterData={filterDrinks} sendState={setState} sendItem={setItem} />
                            : <CategorySelect language={lang} category={category} title={title} filterData={filterDrinks} sendState={setState} sendCat={setCategory} sendItem={setItem} sendOpen={setFilterPopup} />
                        }
                    </div>
                    <CheckoutFooter language={lang} cartFill={cartFill} sendState={setState} />
                  </>
    };

    return (
        <div className="container disable-select">
            {/* { filterPopup && <FilterPopup filterDrinks={filterDrinks} sendFilter={setFilterDrinks} sendOpen={setFilterPopup} /> } */}
            { !isLocked && state === "itemFlow" && <ItemPopup language={lang} item={item} cartNum={cartNum} sendState={setState} sendOrder={addToCart} /> }
            { isLocked ? <LockScreen /> : content }
            { DEVICE !== "CART" && <PythonWebsocket print={printOrder} /> }
        </div>
    );
}

