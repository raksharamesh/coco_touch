import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';


const root = ReactDOM.createRoot(document.getElementById('root'));

// wrap the App component with a wrapper to detect the page clicks
function AppWrapper() {
  const [isClicked, setIsClicked] = useState(false);
  const [resetTimer, setResetTimer] = useState(null);

  // check whether the page is clicked
  function handlePageClick() {
      setIsClicked(true);

      // Reset the timer on every click
      clearTimeout(resetTimer);
      const newResetTimer = setTimeout(() => {
        // reset the click status
        setIsClicked(false); 
      }, 60000); // 1 minute
      // }, 1000); // test

      setResetTimer(newResetTimer);
  };

  // Set up a useEffect to clear the timer when the component unmounts
  useEffect(() => {
      return () => {
        clearTimeout(resetTimer);
      };
  }, [resetTimer]);


  return (
      <div onClick={handlePageClick}>
        <App active={isClicked} />
      </div>
  );
};

root.render(
  <AppWrapper />
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();